<? include ("../Config/testalib.php");
vacio ($idUsr);
session_start();
if (session_is_registered('activa'))
	$activausr = 1;
else
	header("Location: ../index.php");
conecta ("avisos");
$estado = estadouser($idUsr);
?>
<html>
<head>
<title>Registro Nacional de Testamentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
function validar() 
{	campocujus = form1.decujus;
	campociudadano = form1.ciudadano;
	var respuesta; 
	respuesta=confirm("Estas seguro que deseas enviar esta información?");
    if (respuesta)
    {  	if (campocujus.value == '' )
		{  	alert("El campo NOMBRE no ha sido capturado y es OBLIGATORIO");
            return false;
        }
		else		
		{	if (campociudadano.value == '' )
			{	alert("El campo CIUDADANO no ha sido capturado y es OBLIGATORIO");
                return false;
			}            
			else
		 	{	return true;	}
		 }	 
	}	   
    else
    	return false;
}
</script>
</head>
<body leftmargin="0" topmargin="0">
<?	include('./head.php'); ?>
<form name="form1" method="post" onSubmit="return validar();" action="./solicita.php?idUsr=<?=$idUsr?>">
<table width="750" height="458" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
	<tr> 
    	<td height="16" colspan="7" bgcolor="#3983C5"><font color="#FFFFFF" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>VALIDACI&Oacute;N 
        DE LA SOLICITUD DE CONSULTA</strong></font></td>
    </tr>
    <tr> 
    	<td height="19" colspan="7">&nbsp;</td>
    </tr>
    <tr> 
      	<td height="54" colspan="7"> <div align="justify"><strong><em> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
        <?
		$sqlquery = "SELECT * FROM user WHERE idUsr =".$idUsr;
  	    $queryresult = mysql_query($sqlquery);
	    $usuar =mysql_fetch_array($queryresult); 
	    echo $usuar["Nombre"]." ".$usuar["apPaterno"]." ".$usuar["apMaterno"].", en mi caracter de ".$usuar["puesto"]." del ".$usuar["dependencia"];
		?>
        </font></em><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><em> 
        , </em>con fundamento en el convenio de coordinaci&oacute;n celebrado 
        entre la Secretar&iacute;a de Gobernaci&oacute;n y la entidad federativa 
        <em> 
        <?
        $sqlquery = "SELECT idEstado,Estado from catestado where idEstado = ".$usuar[idEstado];
		$queryresult = mysql_query($sqlquery);
		$datos=mysql_fetch_array($queryresult); 
		echo $datos["Estado"];
		mysql_close();
		?>
        ,</em> solicito a usted se sirva informar al suscrito si en esa Direcci&oacute;n a su cargo se encuentra informaci&oacute;n de que quien en vida llev&oacute; 
        el nombre de </font><font size="4"> 
        <input name="decujus" type="text" id="cuyus2" size="60" tabindex="1" value="<?=$Nombre." ".$apPaterno." ".$apMaterno;?>">
        </font><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>, 
        haya otorgado o depositado testamento.</strong></font> <font size="4"> 
        </font></strong></div></td>
    </tr>
    <tr> 
      <td height="22" colspan="7"><font size="4">&nbsp; </font></td>
    </tr>
    <tr> 
      <td height="16" colspan="7"> <div align="justify"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>La 
          presente es motivada por la petici&oacute;n formulada por el C. </strong></font><font size="4"> 
          <input name="ciudadano" type="text" id="ciudadano2" size="45" tabindex="2">
          <strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">(juez 
          o notario, nombre y n&uacute;mero o, cuando corresponda, particular que acredite inter&eacute;s jur&iacute;dico), ante quien se tramita 
          el procedimiento sucesorio respectivo, en su caso, conforme a los siguientes datos:</font></strong></font></div></td>
    </tr>
    <tr> 
      <td height="22" colspan="7"><font size="4">&nbsp; </font></td>
    </tr>
    <tr> 
      <td width="165" height="32">&nbsp;</td>
      <td width="166" ><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">N&uacute;mero 
        de expediente:</font></strong></td>
      <td width="126"><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
        <input name="expediente" type="text" id="expediente" size="20" tabindex="3">
        </font></strong></td>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr> 
      <td height="35">&nbsp;</td>
      <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Forma 
        de acreditar el inter&eacute;s jur&iacute;dico:</font> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br>
        </font></strong></td>
      <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><br>
        </font></strong></td>
      <td colspan="3"><p>&nbsp; </p></td>
    </tr>
    <tr> 
      <td height="46">&nbsp;</td>
      <td><input name="acreditar" type="radio" value="radiobutton" tabindex="4" checked> <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Juez</font> 
      </td>
      <td><input type="radio" name="acreditar" value="radiobutton" tabindex="5"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Notario</font></td>
      <td colspan="2"><div align="justify"> 
          <input type="radio" name="acreditar" value="radiobutton" tabindex="6">
          <font size="2" face="Verdana, Arial, Helvetica, sans-serif">Otra que 
          proceda conforme a la legislaci&oacute;n local , <strong>ACREDITANDO</strong> 
          la defunci&oacute;n.</font></div></td>
    </tr>
    <tr> 
      <td colspan="7"><font size="4">&nbsp;</font></td>
    </tr>
    <tr> 
      <td colspan="7"></td>
    </tr>
    <tr> 
      <td colspan="7"></td>
    </tr>
    <tr> 
      <td colspan="7"></td>
    </tr>
    <tr> 
      <td colspan="7"></td>
    </tr>
    <tr> 
      <td height="40" colspan="7" bgcolor="#E8F1F8"> <div align="justify"><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>Bajo 
      mi m&aacute;s formal protesta de decir verdad, manifiesto que qued&oacute; 
      debidamente acreditado que la persona respecto de la cual solicito informaci&oacute;n ya falleci&oacute;.</strong></font></div></td>
    </tr>
    <tr> 
      <td height="17" colspan="7" bgcolor="#E8F1F8"></td>
    </tr>
    <tr> 
      <td height="17" colspan="2" bgcolor="#E8F1F8">&nbsp;</td>
      <td bgcolor="#E8F1F8"><div align="right"> </div></td>
      <td width="144" bgcolor="#E8F1F8"><div align="right"> 
      <input name="botonvalidar" type="submit" id="validar2" value="VALIDAR" tabindex="7"></div></td>
      <td width="147" bgcolor="#E8F1F8"><div align="right"> 
      <input type="button" name="cancelar" value="CANCELAR" tabindex="44" onClick="history.back(-1)" tabindex="8"></div></td>
      <td width="1">&nbsp;</td>
      <td width="1" height="17"></td>
    </tr>
  </table>
</td>
    </tr>
  </table>
  <p>&nbsp;</p>
</form>
</body></html>