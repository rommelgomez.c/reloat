<?
include "../Config/testalib.php";  //Se incluye el archivo con las diferentes funciones que se utilizan 
include "../Config/KXFormDBBased.class.php";  //Se incluye el archivo con las clases para la creaci�n de los campos del formulario

session_start();
	
if (session_is_registered('activa'))
	$activausr = 1;

else
	header("Location: ../index.php");
conecta("avisos");  //Funcion para conectarse a la base de datos
$estado=estadouser($idUsr);  //Funcion para obtener el estado al que pertenece el usuario
/*echo $idNotario;
echo $tipoDep*/

?>
<html>
<head>
<title>Registro Local de Avisos de Testamento</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="./scripts/Stylesheetpe.css" rel="stylesheet" type="text/css">
<style type="text/css"><!--
.Estilo1 {font-size: 12px}
--></style>
<!-- Se incluyen los archivos para las m�scaras de los campos de texto -->
<script src="scripts/mask/masks.js"></script>
<script src="scripts/mask/config.js"></script>
</head>
<script language="JavaScript">
//Pregunta al usuario si desea borrar el registro
function validar() 
{	var respuesta; 
	respuesta=confirm("Estas seguro que deseas borrar este registro?");
    if (respuesta)
    {	return true;	}
	else
	{	return false;	}	
}

//Bloquea o desbloquea algunos campos del formulario
function bloquea()
{	form = document.form1;
	
	if (form.bpor.value == 2)
	{	form.nombre.disabled = true;
		form.appaterno.disabled = true;
		form.apmaterno.disabled = true;
		form.nombre.value = "";
		form.appaterno.value = "";
		form.apmaterno.value = "";
	}
	else
	{	form.nombre.disabled = false;
		form.appaterno.disabled = false;
		form.apmaterno.disabled = false;
	}
}

//Convierte a mayusculas lo que se escriba en los campos de texto
function Mayusculas(nombre)
{	var alta= new String();
    alta=nombre.value;
	alta= alta.replace(/(^\s*)|(\s*$)/g,""); 	
    nombre.value=alta.toUpperCase();
}
</script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p>
<?	include('./head.php');	//Se incluye el banner del encabezado

$fdb = new KXFormDBBased("testamentos"); //Creamos el objeto fdb para la tabla de testamentos	

// ARREGLOS PARA LLENAR LOS SELECTS
$tipo_busq = array();
	$tipo_busq["0"]["label"] = "Escritura"; 				$tipo_busq["0"]["value"] = "1";
	$tipo_busq["1"]["label"] = "No. Control RELOAT";		$tipo_busq["1"]["value"] = "2";
	$tipo_busq["2"]["label"] = "No. Control de la Dependencia";		$tipo_busq["2"]["value"] = "3";


// GENERA CAMPOS DEL FORMULARIO
$bpor = $fdb->addSelectList("bpor",$tipo_busq,"",array(),'onChange="bloquea()"');
$nombre = $fdb->addTHPInput("nombre","","text","",40,'onChange="Mayusculas(this)"');
$appaterno = $fdb->addTHPInput("appaterno","","text","",40,'onChange="Mayusculas(this)"');
$apmaterno = $fdb->addTHPInput("apmaterno","","text","",40,'onChange="Mayusculas(this)"');

$tipoDosVal = $_SESSION['tipoDep'];
?>
</p>
<form name="form1" method="post" action="./buscaescritura.php?idUsr=<?=$idUsr;?>&bescritura=<?=$bescritura;?>">
<table width="750" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
	<tr bgcolor="#663300"> 
    	<td colspan="5" bgcolor="#3983C5"><font color="#FFFFFF"><strong>RECTIFICACI&Oacute;N <? if($tipoDosVal == 'DEPENDENCIA'){ ?>Y ELIMINACI&Oacute;N<? }?> DE REGISTROS</strong></font></td>
    </tr>
    <tr> 
    	<td colspan="5">&nbsp;</td>
    </tr>
    
    <tr> 
    	<td colspan="5">&nbsp;</td>
    </tr>
    <tr> 
    	<td width="198" height="22">Nombre: <?=$nombre;?></td>
    	<td width="112" align="right"><strong>Apellido Paterno:</strong> </td>
   	  	<td width="73"><?=$appaterno;?></td>
  	  	<td width="194" align="right"><strong>Apellido Materno:</strong> </td>
  	  <td width="158"><?=$apmaterno;?></td>
    </tr>
    <tr> 
   	  	<td width="198" height="22">Buscar por: <?=$bpor;?></td>
    	<td width="112" align="right"><strong>Valor a Buscar: </strong></font></td>
  	  	<td width="73"><input name="bescritura" type="text" id="bescritura"  OnKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=48 && event.keyCode <= 57){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = false;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==45){event.returnValue = true;} else{event.returnValue = false;}"></td>
  	  	<td width="194" align="center"><input type="submit" name="Submit" value="BUSCAR"></td>
      	<td width="158">&nbsp;</td>
    </tr>
    <tr> 
      	<td colspan="5">&nbsp;</td>
    </tr>    
</table>
</form>
<p> 
<? 
if (isset($bescritura))
{	//Hacemos la consulta seg�n los datos escritos
	$sqlquery = "SELECT idTestamento, escritura, Nombre, apPaterno, apMaterno, fechaIngreso 
				 FROM testamentos WHERE idEstado = ".$estado['idEstado'];
	$qryanexo1='';$qryanexo2='';$qryanexo3='';$qryanexo4='';$qryanexo6='';$qryanexo5='';					 	
	if ($_POST["nombre"] != "")
	{	$qryanexo1 = " AND Nombre = '".$_POST["nombre"]."'";	  }	
		
	if ($_POST["appaterno"] != "")
	{	$qryanexo2 = " AND apPaterno = '".$_POST['appaterno']."'";	}
			
	if ($_POST["apmaterno"] != "")
	{	$qryanexo3 = " AND apMaterno = '".$_POST['apmaterno']."'";	}
		
	if ($_POST["bpor"] == 1)
	{
		if ($_POST["bescritura"] != "")
		{	$qryanexo4 = " AND escritura = ".$_POST['bescritura'];	}
	}
		
	if ($_POST["bpor"] == 2)
	{
		if ($_POST["bescritura"] != "")
		{	$qryanexo4 = " AND procNumInt = ".$_POST['bescritura'];	}
	}
	
	if ($_POST["bpor"] == 3)
	{
		if ($_POST["bescritura"] != "")
		{	$qryanexo4 = " AND procNumIntEdo = ".$_POST['bescritura'];	}
	}
		if ($tipoDep == "NOTARIA")
	{	$qryanexo6 = " AND idNotario = '$idNotario'";	  }	
	
	$qryanexo5=" ORDER BY escritura ASC, Nombre ASC, fechaIngreso ASC";		
	$sqlquery.="$qryanexo1 $qryanexo2 $qryanexo3 $qryanexo4 $qryanexo6 $qryanexo5";		
	//echo $sqlquery;
		
	$bescritura = 1;	   	
	$_pagi_sql = $sqlquery;
}
$_pagi_cuantos = 50;
$_pagi_nav_num_enlaces = 6;
$flag = 1;
//Incluimos el script de paginaci�n. �ste ya ejecuta la consulta autom�ticamente
if (isset($bescritura))
	{include("paginator.inc.php");}	
else	 
{  	if (isset($seg))
   include("paginator.inc.php");
}			


if (isset($_pagi_totalReg))
{   if ($_pagi_totalReg != 0)
    {	//Imprime dentro de la misma p�gina los resultados de la b�squeda
		echo "<font color='#FF0000' size='2' face='Verdana, Arial, Helvetica, sans-serif'>&nbsp; <b>N�mero de coincidencias: " .  $_pagi_totalReg ;
		echo "</b></font><table border=1 width=750>";
		echo " <tr>";
		echo "  <td><center><b>ESCRITURA</b></center></td>\n";
		echo "  <td><center><b>NOMBRE DEL TESTADOR</b></center></td>\n";
		echo "  <td><center><b>FECHA DE INGRESO</b></center></td>\n";
if($tipoDep != "NOTARIA"){		
		echo "  <td><center><b>ELIMINAR</b></center></td>\n"; 
	}
		echo "  <td><center><b>MODIFICAR</b></center></td>\n";
		echo "  </tr>\n";
		while ($dato=mysql_fetch_array($_pagi_result)) 
		{	echo "  <tr>\n";
			echo "    <td><center>".$dato["escritura"]."</center></td>\n";
			echo "    <td>".$dato["Nombre"]. " " .$dato["apPaterno"]." ".$dato["apMaterno"]."</td>\n";
		 	echo "    <td><center>".$dato["fechaIngreso"]."</center></td>\n";
           
		    //Boton de borrar (si es notario no aparece)
		
		if($tipoDep != "NOTARIA")
		{
			echo "<Form method=POST action=fverificacion.php onSubmit=\"return validar();\">";
            echo "<td><center><input type = hidden name=id value=".$dato["idTestamento"].">";
			echo "<input type = hidden name=bescritura value=".$dato["escritura"].">";
			echo "<input type=\"submit\" name=\"eliminar\" value=\"ELIMINAR\"></center></td>";
			echo"</form>";
		}
			//Boton de modificar
			echo "<Form method=POST action=fmodificacion.php?flag=".$flag.">";
            echo "<td><center><input type = hidden name=id value=".$dato["idTestamento"].">";
			echo "<input type=\"submit\" name=\"modificar\" value=\"RECTIFICAR\"></center></td>";
			echo"</form>";
			echo "  </tr>\n";
		}
	echo "</table>\n";

	}}
	if(isset($_pagi_navegacion))
	{ echo"<center><p><font color=black>".$_pagi_navegacion."</p></center>"; }
	mysql_close ();	?>
</p><br>
<table width="750" height="15" border="0" cellpadding="0" cellspacing="0">
  <tr bgcolor="#E8E8E8">
    <td><center>&nbsp; <a href="./validacion/menu.php">Men&uacute;</a></center></td>   

</tr>  
</table>
<? include ('./foot.php'); //Se incluye el banner para el pie de p�gina?>
<p align="left">&nbsp;</p>
</body>
</html>