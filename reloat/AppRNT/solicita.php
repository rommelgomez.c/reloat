<? include ("../Config/testalib.php");	//Se incluye el archivo con las diferentes funciones que se utilizan 
   include ("../Config/KXFormDBBased.class.php");	//Se incluye el archivo con las clases para la creaci�n de los campos del formulario
session_start();
if (session_is_registered('activa'))
	$activausr = 1;
else
	header("Location: ../index.php");
conecta("avisos");  //Funcion para conectarse a la base de datos

/*echo "Usuario Sesion: ".$uid;
echo "Usuario Alta: ".$idUsr;*/
?>
<html>
<head>
<title>Registro Local de Avisos de Testamentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="./scripts/Stylesheetpe.css" rel="stylesheet" type="text/css">
<script src="js/dw_event.js" type="text/javascript"></script>
<script src="js/dw_viewport.js" type="text/javascript"></script>
<script src="js/dw_tooltip.js" type="text/javascript"></script>
<script src="scripts/mask/config.js"></script>
<script src="scripts/mask/masks.js"></script>
<script>
//Para los datos del que acredita
var ns4 = (document.layers);
var ie4 = (document.all && !document.getElementById);
var ie5 = (document.all && document.getElementById);
var ie6 = (document.all && document.getElementById);
var ns6 = (!document.all && document.getElementById);
var ns7 = (!document.all && document.getElementById);
hide = 1;

//Valida que se llenen los campos necesarios para la b�squeda
function validar()
{	var respuesta;
 	var errores='';
 	var errores1='Se encontraron los siguientes errores\n';
	errores1+='______________________________________________________________________ \n';
  	if (document.form1.Nombre.value == '')
		errores+='El campo Nombre(s) no ha sido capturado y es OBLIGATORIO\n';
  	if (document.form1.apPaterno.value == '' && document.form1.apMaterno.value == '')
	 	errores+='El campo Apellido Paterno o Apellido Materno no han sido capturados, especifique al menos uno\n';
  	if (document.form1.nacional.checked == true)
	{	if (document.form1.acreditar.selectedIndex == 0){
	 		errores+='Debe seleccionar la persona que acredita y es OBLIGATORIO\n';
			document.form1.acreditar.focus();
		}else{
						
		  	if (document.form1.acreditar.value == 'juez')
			{	
				if(document.form1.tipoJuez.value == '')
		   			errores+='El campo Tipo no ha sido capturado y es OBLIGATORIO\n';
				if(document.form1.munJuez.value == '')
		   			errores+='El campo Municipio o localidad no ha sido capturado y es OBLIGATORIO\n';
				if(document.form1.expJuez.value == '')
		   			errores+='El campo N�mero de expediente no ha sido capturado y es OBLIGATORIO\n';   
				if(document.form1.juez.value == '')
					errores+='El campo Nombre del Juez no ha sido capturado y es OBLIGATORIO\n';
			  	if(document.form1.juez.value.length>0 && document.form1.juez.value.length<8)
					errores+='El campo Nombre del Juez debe tener m�nimo 8 letras.\n';	
	 		}
	 		if (document.form1.acreditar.value == 'notario')
			{	if (document.form1.numeroNot.value == '')
		   			errores+='El campo N�mero de notario no ha sido capturado y es OBLIGATORIO\n';
				if(document.form1.notario.value == '')
					errores+='El campo Nombre del Notario no ha sido capturado y es OBLIGATORIO\n';
			  	if(document.form1.notario.value.length>0 && document.form1.notario.value.length<8)
					errores+='El campo Nombre del Notario debe tener m�nimo 8 letras.\n';	
			}	
	 		if (document.form1.acreditar.value == 'otroad')				
			{	
				
				if(document.form1.ciudadano2.value == '' || document.form1.ciudadano2.value.length <= 0){
					errores+='El campo Nombre del Solicitante no ha sido capturado y es OBLIGATORIO\n';
				}
				if(document.form1.ciudadano2.value.length>0 && document.form1.ciudadano2.value.length<8){
					errores+='El campo Nombre del Solicitante debe tener m�nimo 8 letras.\n';
				}
				if(document.form1.anosol.value.length > 0 && document.form1.anosol.value.length < 4){
		   			errores+='El campo A�o debe ser de 4 d�gitos\n';
				}	
						
			  	
	 		}
		}
	}	  	
	   
  	if(errores)
	{	alert(errores1+=errores);
	 	return false;
  	}
	else
	{	respuesta=confirm("Estas seguro que deseas enviar esta informaci�n?");
     	if(respuesta)
	    	return true;
	 	else
	    	return false;	
	}
}	 

//Funci�n Mayusculas: Convierte a mayusculas lo que se escriba en los campos de texto
function Mayusculas(nombre)
{	var alta= new String();
    alta=nombre.value;
	alta= alta.replace(/(^\s*)|(\s*$)/g,""); 	
    nombre.value=alta.toUpperCase();
}

//Hace visible o invisible el formulario para la b�squeda nacional
function cambiaLink()
{	form = document.form1;	
	if (form.nacional.checked == true)
	{	visible('busqueda','on');		
		form.acreditar.disabled = false;				
	}	
	else
	{	if (form.nacional.checked == false)
		{	visible('busqueda','');			
			form.acreditar.disabled = true;
			visible('tabla1','');
			visible('tabla2','');
			visible('tabla3','');			
			form.acreditar.value = 0; 				
		}
	}		
}

//Funci�n Visible: Hace que un objeto este visible o invisible a los ojos del usuario
function visible(objeto,on)
{	if (on)
	{  	// Netscape 4
		if(ns4){
			document.layers[objeto].visibility = "show";
		}
		// Explorer 4
		else if(ie4){
			document.all[objeto].style.visibility = "visible";
		}
		// W3C - Explorer 5+ and Netscape 6+
		else if(ie5 || ie6 || ns6 || ns7){
			document.getElementById(objeto).style.visibility = "visible";
		}
  	}
	else
	{  	// Netscape 4
		if(ns4){
			document.layers[objeto].visibility = "hide";
		}
		// Explorer 4
		else if(ie4){
			document.all[objeto].style.visibility = "hidden";
		}
		// W3C - Explorer 5+ and Netscape 6+
		else if(ie5 || ie6|| ns6 || ns7){
			document.getElementById(objeto).style.visibility = "hidden";
		}
  	}
}

function doTooltip(e, msg) {
  if ( typeof Tooltip == "undefined" || !Tooltip.ready ) return;
  Tooltip.show(e, msg);
}

function hideTip() {
  if ( typeof Tooltip == "undefined" || !Tooltip.ready ) return;
  Tooltip.hide();
}

//Muestra los campos a llenar seg�n la forma de acreditaci�n del aviso
function mostrar(campo)
{	
	document.form1.numeroJuez.value="";
	document.form1.tipoJuez.value="";
	document.form1.munJuez.value="";
	document.form1.expJuez.value="";
	document.form1.juez.value="";
	
	document.form1.numeroNot.value="";
	document.form1.munNotario.value="";
	document.form1.notario.value="";
	
	document.form1.ciudadano2.value="";
	document.getElementById("diasol").selectedIndex = "0";
	document.getElementById("messol").selectedIndex = "0";	
	document.form1.anosol.value="";
	document.form1.doctoParticular.value="";
	
	
	
	if(campo == 0)
	{	
		visible('tabla1','');
		visible('tabla2','');
		visible('tabla3','');		
 	}
	else 
	{	if(campo == 'juez')
		{							
			visible('tabla1','on');			
			visible('tabla2','');
			visible('tabla3','');			
  		}
		else
		if(campo == 'notario')
		{	
			visible('tabla2','on');	  			
			visible('tabla1','');
			visible('tabla3','');			
  		}
		else
		if(campo == 'otroad')
		{	
			visible('tabla3','on');	  
			visible('tabla1','');
			visible('tabla2','');			
	  	}	
 	}
}

//Verifica que se llenen los campos necesarios para agregar los alias a la b�squeda
function verifica()
{	if (document.form1.nalias.value == "") 
   	{  	alert ('No hay nombre de "Tambien conocido como"!');
     	return false;
   	}
	else
	{	if ((document.form1.apalias.value == "") && (document.form1.amalias.value == ""))
    	{	alert ('No hay ningun apellido de "Tambien conocido como"!');
     		return false;
    	}
		valor = document.form1.nalias.value + " " + document.form1.apalias.value + " " + document.form1.amalias.value  + " " + document.form1.acalias.value;
		if (document.form1.listaa.value == "")
			document.form1.listaa.value = valor;
		else
			document.form1.listaa.value = document.form1.listaa.value + "\n" + valor;

		switch (hide)
		{	<? for ($x=0;$x<=9;$x++)
      		{?>
  				case <? echo($x); ?>:
				noalias<?=$x?>=document.form1.nalias.value;
    			document.form1.nalias<?=$x?>.value=noalias<?=$x?>;
    			document.form1.apalias<?=$x?>.value=document.form1.apalias.value;
   				document.form1.amalias<?=$x?>.value=document.form1.amalias.value;
				document.form1.acalias<?=$x?>.value=document.form1.acalias.value;
				break ;	<? 
			} 	?>
		}
		hide=hide+1;
		document.form1.apalias.value="";
		document.form1.amalias.value="";
		document.form1.nalias.value="";
		document.form1.acalias.value="";
		return false;
	}
}

//Pone vac�os los campos de nombre y apellidos del alias
function ocultar()
{	document.form1.nalias.value = '';
	document.form1.apalias.value = '';
	document.form1.amalias.value = '';
	document.form1.acalias.value = '';
	document.form1.listaa.value = '';
}

function abrir(pagina) {
	window.open(pagina,'window','params');
}

</script>


<style type="text/css">
<!--
#busqueda {position:absolute; left:1px; top:315px;	width:119px; height:104px; z-index:0; visibility:hidden;}
#tabla1 {position: absolute; left:390px; top:460px; width:325px; height:104px; z-index:1; visibility:hidden;}
#tabla2 {position: absolute; left:390px; top:460px; width:325px; height:104px; z-index:1; visibility:hidden;}
#tabla3 {position: absolute; left:390px; top:460px;	width:325px; height:104px; z-index:1; visibility:hidden;}
-->
</style>
</head>
<? //Genera campos del formulario
$fdb = new KXFormDBBased("testamentos");	 
$nomPadre = $fdb->addTHPInput("nomPadre","","text","",60,'tabindex="14" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
$apPatPadre = $fdb->addTHPInput("apPatPadre","","text","",30,'tabindex="15" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
$apMatPadre = $fdb->addTHPInput("apMatPadre","","text","",30,'tabindex="16" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
$nomMadre = $fdb->addTHPInput("nomMadre","","text","",60,'tabindex="17" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
$apPatMadre = $fdb->addTHPInput("apPatMadre","","text","",40,'tabindex="18" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
$apMatMadre = $fdb->addTHPInput("apMatMadre","","text","",40,'tabindex="19" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
$apConMadre = $fdb->addTHPInput("apConMadre","","text","","",'tabindex="20" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
?>
<body leftmargin="0" topmargin="0">
<form name="form1" method="post" onSubmit="return validar();" action="icertificacion.php" >
<?	include('./head.php');  //Se incluye el banner para el encabezado?><br>
<table width="750" border="0" cellpadding="0" cellspacing="0">
<tr> 
   <td width="78%" bgcolor="#3983C5"><div align="left"><b align="left"><strong><font color="#FFFFFF">B&Uacute;SQUEDA DE AVISOS DE TESTAMENTO</font></strong></b></div></td>
</tr>
<tr> 
   <td bgcolor="#BAD2EA" align="left"><strong> Datos del Testador </STRONG>
     <table width="100%" border="0" bgcolor="#E8E8E8">
       <tr>
         <td height="25" bgcolor="#E8E8E8" colspan="2"><font color="#FF0000">*Campos obligatorios</font></td>
         <td width="481" bgcolor="#E8E8E8" align="center"><input name="alias" type="button" value="Tambi&eacute;n conocido como:" onClick="javascript:visible('talias',true);" style="background-color:#333333; color:#FFFFFF; font-weight:bold;"/></td>
         <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="./pdf/folleto.pdf" target="_blank"><img src="./images/help.png" width="35" height="35" alt="Ayuda para consultas"  border="0"></a></td>
       </tr>
       <tr>
         <td width="143"><strong>Nombre(s)<font color="#FF0000">*</font>&nbsp;&nbsp;&nbsp;&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp; </td>
         <td bgcolor="#E8E8E8"><input name="Nombre" tabindex="1" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
         <td rowspan="6">
	<div id="talias" style="visibility:hidden; border:double; border-width:0px; top: 203px; left: 458px;"> 
	<table border="0" align="center">
        <tr> 
          <td>Nombre(s)</td>
          <td><input name="nalias" tabindex="7" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
          <td rowspan="5" valign="top"><textarea name="listaa" tabindex="11" rows="10" type="TextArea" wrap="OFF" readonly></textarea></td>          
        </tr>
        <tr> 
          <td>Apellido Paterno</td>
          <td><input name="apalias" tabindex="8" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
        </tr>
        <tr> 
          <td>Apellido Materno</td>
          <td><input name="amalias" tabindex="9" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
        </tr>
        <tr> 
          <td>Apellido C�nyuge</td>
          <td><input name="acalias" tabindex="10" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"></td>
        </tr>
        <tr> 
          <td colspan="2" align="left"><input name="add" tabindex="12" type="button" value="Agregar" onClick="return verifica();"/>
          &nbsp; <input name="alias" tabindex="13" type="button" value="Ocultar" onClick="javascript:visible('talias',''); ocultar();"/></td>
        </tr>		       
      </table></div></td>
       </tr>
       <tr>
         <td> Apellido Paterno<font color="#006600"> *</font> <strong> &nbsp;&nbsp; </strong> &nbsp; </td>
         <td bgcolor="#E8E8E8"><input name="apPaterno" tabindex="2" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
       </tr>
       <tr>
         <td><strong> Apellido Materno<font color="#006600"> *</font> </strong> </td>
         <td bgcolor="#E8E8E8"><input name="apMaterno" tabindex="3" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
       </tr>
       <tr>
         <td><strong>Apellido C&oacute;nyuge</strong> </td>
         <td bgcolor="#E8E8E8"><input name="apConyuge" tabindex="4" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
       </tr>
       <tr>
         <td><strong>CURP</strong> </td>
         <td bgcolor="#E8E8E8"><input name="curp" tabindex="5" size="19" maxlength="18" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=48 && event.keyCode <= 57){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = false;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else{event.returnValue = false;}"/></td>
       </tr>
       <tr>
         <td>B&uacute;squeda Nacional</td>
         <td bgcolor="#E8E8E8"><input type="checkbox" name="nacional" tabindex="6" id="nacional" onClick="cambiaLink()"/></td>
       </tr>         
       <tr>
         <td colspan="2"><div id="busqueda">
             <table width="750" border="0" bgcolor="#E8E8E8">
             	<tr bgcolor="#BEB094"> 
    				<td colspan="5" bgcolor="#BAD2EA">DATOS DE LOS PADRES</td>
  				</tr>
  				<tr> 
    				<td colspan="2">PADRE</td>
    				<td width="37">&nbsp;</td>
    				<td colspan="2">MADRE</td>
  				</tr>
  				<tr>
    				<td width="141">Nombre(s)</td>
    				<td width="199"><font size="3" face="Arial, Helvetica, sans-serif"><?=$nomPadre;?></FONT></td>
				    <td width="37">&nbsp;</td>
				    <td width="132">Nombre(s)</td>
				    <td width="219"><font size="3" face="Arial, Helvetica, sans-serif"><?=$nomMadre;?></FONT></td>
			  	</tr>
  				<tr>
    				<td>Apellido Paterno</td>
    				<td><font size="3" face="Arial, Helvetica, sans-serif"><?=$apPatPadre;?></FONT></td>
				    <td>&nbsp;</td>
				    <td>Apellido Paterno</td>
				    <td><font size="3" face="Arial, Helvetica, sans-serif"><?=$apPatMadre;?></FONT></td>
	  			</tr>
  				<tr>
    				<td>Apellido Materno</td>
    				<td><font size="3" face="Arial, Helvetica, sans-serif"><?=$apMatPadre;?></FONT></td>
    				<td>&nbsp;</td>
    				<td>Apellido Materno</td>
    				<td><font size="3" face="Arial, Helvetica, sans-serif"><?=$apMatMadre;?></FONT></td>
  				</tr>
  				<tr>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
    				<td>&nbsp;</td>
    				<td>Apellido C&oacute;nyuge</td>
    				<td><?=$apConMadre;?></td>
  				</tr>
			</table><br>
             <table width="380" border="0">               
               <tr>
    			<td>Nacionalidad</td>
    			<td><input name="nacionalidad" tabindex="21" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else{event.returnValue = false;}"/></td>
  			   </tr>
  			   <tr>
    			<td>Lugar de Nacimiento</td>
    			<td><input name="lugarNacimiento" tabindex="22" size="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode ==34 || event.keyCode ==39 || event.keyCode ==45 || event.keyCode ==92){event.returnValue = false;}"/></td>
  			   </tr>  		 	  
  			   <tr>
    			<td>Fecha de Nacimiento</td>
      			<td><font size="3" face="Arial, Helvetica, sans-serif"><select name="dia" tabindex="23">
          			<option value="0" selected>Dia</option>
          			<? //Llena el select de los d�as con valores del 1 al 31
					   for ($x=1; $x<=31; $x++)
					   {	if ($x<10)
					   			$d = "0".$x;
							else
								$d = $x;
							echo "<option value='$d'>$d</option>";
						}
					?>          			
        		</select> /
		        <select name="mes" tabindex="24">
        		  <option value="0" selected>Mes</OPTION>
		          <option value="01">Enero</OPTION>
        		  <option value="02">Febrero</OPTION>
		          <option value="03">Marzo</OPTION>
        		  <option value="04">Abril</OPTION>
		          <option value="05">Mayo</OPTION>
        		  <option value="06">Junio</OPTION>
		          <option value="07">Julio</OPTION>
        		  <option value="08">Agosto</OPTION>
		          <option value="09">Septiembre</OPTION>
        		  <option value="10">Octubre</OPTION>
		          <option value="11">Noviembre</OPTION>
        		  <option value="12">Diciembre</OPTION>
        		</SELECT>
                <input name="anio"  tabindex="25" type="text" size="5" maxlength="4" onFocus="integerMask(this)"/>

        		</FONT></td>
  			   </tr>
  			   <tr>
    			<td>Estado Civil</td>
    			<td><select name="edoCivil" tabindex="26">
        			<option value="0" selected>Seleccione</option>
        			<option value="SOLTERO">Soltero</option>
        			<option value="CASADO">Casado</option>
        			<option value="DIVORCIADO">Divorciado</option>
        			<option value="VIUDO">Viudo</option>
        			<option value="UNION LIBRE">Uni&oacute;n Libre</option>
        			<option value="SOC. DE CONVIVENCIA U HOMOLOGA">Soc. de Convivencia u Hom&oacute;loga</option>
      				</select>
      			</td>      
  			   </tr>
   			   <tr>
    			<td>Fecha de Fallecimiento</td>
      			<td><font size="3" face="Arial, Helvetica, sans-serif">
                <select name="diaf" tabindex="27">
          			<option value="0" selected>Dia</option>
         			<? //Llena el select de los d�as con valores del 1 al 31
					   for ($x=1; $x<=31; $x++)
					   {	if ($x<10)
					   			$d = "0".$x;
							else
								$d = $x;
							echo "<option value='$d'>$d</option>";
						}
					?>    
        		</select> /                 
		        <select name="mesf" tabindex="28">
        		  <option value="0" selected>Mes</OPTION>
		          <option value="01">Enero</OPTION>
        		  <option value="02">Febrero</OPTION>
		          <option value="03">Marzo</OPTION>
        		  <option value="04">Abril</OPTION>
		          <option value="05">Mayo</OPTION>
        		  <option value="06">Junio</OPTION>
		          <option value="07">Julio</OPTION>
        		  <option value="08">Agosto</OPTION>
		          <option value="09">Septiembre</OPTION>
        		  <option value="10">Octubre</OPTION>
		          <option value="11">Noviembre</OPTION>
        		  <option value="12">Diciembre</OPTION>
        		</SELECT> / 
                <input name="aniof" size="5" tabindex="29" maxlength="4" onFocus="integerMask(this)">
        		</FONT></td>
 		 	  </tr>
               <tr>
                 <td width="197">B&uacute;squeda Motivada a Petici&oacute;n de <font color="#FF0000"> *</font></td>
                 <td width="243" bgcolor="#E8E8E8"><select name="acreditar" onChange="mostrar(this.value);" tabindex="30">
                   <option value='0' selected>Seleccionar</option>
                   <option value='juez'>Juez</option>
                   <option value='notario'>Notario</option>
                   <!--<option value='otroad'>Particular</option>-->
                 </select></td>
               </tr>
             </table>
         </div></td>
       </tr>
       <tr>
         <td valign="top"><span id="tabla1">
           <table width="310" border="0">
               <tr>
                 <td colspan="2">DATOS DEL JUEZ</td>
               </tr>
               <tr>
                 <td>No. de Juez</td>
                 <td><input name="numeroJuez" tabindex="31" type="text" onMouseOver="doTooltip(event,'Indicar el n&uacute;mero de Adscripci&oacute;n del Juez')" onMouseOut="hideTip()" size="2" maxlength="3" OnKeyPress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;"/></td>
               </tr>
               <tr>
                 <td>Tipo<font color="#FF0000"> *</font></td>
                 <td><input name="tipoJuez" tabindex="32" type="text" size="7" onMouseOut="hideTip()" onMouseOver="doTooltip(event,'(Ej. CIVIL, FAMILIAR, MIXTO, PENAL,   ETC.)')" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else{event.returnValue = false;}"/></td>
               </tr>
               <tr>
                 <td>Municipio o localidad de adscripci&oacute;n<font color="#FF0000"> *</font></td>
                 <td><input name="munJuez" tabindex="33" type="text" size="7" onMouseOut="hideTip()" onMouseOver="doTooltip(event,'Municipio de Adscripci&oacute;n del Juez')" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else{event.returnValue = false;}"/></td>
               </tr>
               <tr>
                 <td>No. de expediente<font color="#FF0000"> *</font></td>
                 <td><input name="expJuez" tabindex="34" type="text" size="7" maxlength="20" onKeyPress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;"/></td>
               </tr>
               <tr>
       	 		 <td>Nombre completo del Juez<font color="#FF0000"> *</font></td>
       	   		 <td><input name="juez" tabindex="35" type="text" id="juez" onChange="Mayusculas(this)" onMouseOver="doTooltip(event,'Indicar Nombre(s) y Apellidos')" onMouseOut="hideTip()" size="30" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
       		   </tr>                 
             </table>
         </span></td>
         <td width="162" valign="top"><span id="tabla2">
           <table width="308">
               <tr>
                 <td colspan="2">DATOS DEL NOTARIO</td>
               </tr>
               <tr>
                 <td>No. de Notario<font color="#FF0000"> *</font></td>
                 <td><input name="numeroNot" tabindex="36" type="text" onMouseOver="doTooltip(event,'Indicar el n&uacute;mero de Adscripci&oacute;n del Notario')" onMouseOut="hideTip()" size="2" maxlength="3" OnKeyPress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;"/></td>
               </tr>
               <tr>
                 <td>Municipio de adscripci&oacute;n</td>
                 <td><input name="munNotario" tabindex="37" type="text" size="9" onMouseOut="hideTip()" onMouseOver="doTooltip(event,'Indicar Localidad donde se ubica la Notar�a')" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else{event.returnValue = false;}"/></td>
               </tr>
               <tr>
       	 		 <td>Nombre completo del Notario<font color="#FF0000"> *</font></td>
       	   		 <td><input name="notario" tabindex="38" type="text" id="notario" onChange="Mayusculas(this)" onMouseOver="doTooltip(event,'Indicar Nombre(s) y Apellidos')" onMouseOut="hideTip()" size="30" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
       		   </tr>                 
             </table>
         </span></td>
         <td valign="top"><span id="tabla3">
	  <table>       
       <tr>
        <td align="justify" style="color:#FF0000; text-align:justify">Autoridad Local a petici&oacute;n de un Particular que haya acreditado inter&eacute;s jur&iacute;dico en t&eacute;rminos de su legislaci&oacute;n local.</font></td>
       </tr>
       <tr>
        <td>DATOS DEL PARTICULAR</td>
       </tr>
       <tr>
         <td>Nombre del solicitante<font color="#FF0000"> *</font><br>
           <input name="ciudadano2" type="text" tabindex="39" size="35" onChange="Mayusculas(this)" onMouseOut="hideTip()" onMouseOver="doTooltip(event,'Nombre completo')" value="" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"/></td>
       </tr>
       <tr>
         <td>Fecha de solicitud</td>
       </tr>
       <tr>
         <td>
         <select name="diasol" id="diasol" onMouseOut="hideTip()" onMouseOver="doTooltip(event,'Fecha de recepci�n del documento mediante el cual solicit� la b�squeda')" tabindex="40">
         <option value="00">D&iacute;a</option>
         <? //Llena el select de los d�as con valores del 1 al 31
		    for($i=1;$i<=31;$i++){
	          if($i<10) 
		         $n= '0'.$i;
		    else
		         $n=$i;
	        print "<option value=$n>$n</option>\n";
	        } ?>
         </select>
         <span class="style3">/
         <select name="messol" id="messol" tabindex="41">
         <option value="00">Mes</option>
         <?  //Llena el select usando un arreglo con los nombres de los meses
		  $meses=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	      $j=0;
	      foreach($meses as $m){
	        $j++;
            if($j<10) 
		       $n= '0'.$j;
		    else
		       $n=$j;
	        print "<option value=$n>$m</option>\n";
	      }	  ?>              
         </select> /
         <input name="anosol" type="text" tabindex="42" size="4" maxlength="4"/>
         </span></td>
       </tr>
       <tr>
         <td>Motivaci&oacute;n de la solicitud</td>
       </tr>
       <tr>
        <td><input name="doctoParticular" tabindex="43" type="text" size="35" onMouseOut="hideTip()" onMouseOver="doTooltip(event,'Documentos que motivaron la aceptaci�n de la solicitud de b�squeda del particular')" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=48 && event.keyCode <= 57){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else{event.returnValue = false;}"/></td>
       </tr>
      </table>
	 </span></td>         
       </tr>      
     </table></div>    </td>
</tr>     
      <tr bgcolor="#e8e8e8" height="150">
        <td>&nbsp;</td>
      </tr>
       <tr bgcolor="#e8e8e8" height="168">        
        <td>&nbsp;</td>
      </tr>
      <tr bgcolor="#e8e8e8">      	          
        <td align="right"><input type="submit" name="solicita" value="ACEPTAR" tabindex="44">          
        <input type="button" name="cancelar" tabindex="45" value="REGRESAR" onClick="document.location='./validacion/menu.php?uid=<?=$idUsr?>'"> &nbsp; &nbsp; </td>
      </tr>
      <tr bgcolor="#e8e8e8" height="10">        
        <td>&nbsp;</td>
      </tr>
    </table>  
      <!-- Campos ocultos para los alias y los id's necesarios en la sesi�n --> 
      <input type="hidden" name="idUsr" value="<?=$idUsr;?>"/>      
      <input type="hidden" name="idEstado1" value="<?=$estado[idEstado];?>"/>
      <input type="hidden" name="idEstado" value="<?=$estado[idEstado];?>"/>
      <input type="hidden" name="capturista" value="<?=$usuar["Nombre"]." ".$usuar["apPaterno"]." ".$usuar["apMaterno"];?>"/>
      <input type="hidden" name="estatus" value="Pendiente"/>
      <input type="hidden" name="nalias1"/>
	  <input type="hidden" name="apalias1"/>
      <input type="hidden" name="amalias1"/>
  	  <input type="hidden" name="acalias1"/>
  	  <input type="hidden" name="nalias2"/>
  	  <input type="hidden" name="apalias2"/>
  	  <input type="hidden" name="amalias2"/>
  	  <input type="hidden" name="acalias2"/>
  	  <input type="hidden" name="nalias3"/>
  	  <input type="hidden" name="apalias3"/>
  	  <input type="hidden" name="amalias3"/>
  	  <input type="hidden" name="acalias3"/>
  	  <input type="hidden" name="nalias4"/>
  	  <input type="hidden" name="apalias4"/>
  	  <input type="hidden" name="amalias4"/>
  	  <input type="hidden" name="acalias4"/>
  	  <input type="hidden" name="nalias5"/>
  	  <input type="hidden" name="apalias5"/>
  	  <input type="hidden" name="amalias5"/>
  	  <input type="hidden" name="acalias5"/>
  	  <input type="hidden" name="nalias6"/>
  	  <input type="hidden" name="apalias6"/>
  	  <input type="hidden" name="amalias6"/>
  	  <input type="hidden" name="acalias6"/>
  	  <input type="hidden" name="nalias7"/>
  	  <input type="hidden" name="apalias7"/>
  	  <input type="hidden" name="amalias7"/>
  	  <input type="hidden" name="acalias7"/>
  	  <input type="hidden" name="nalias8"/>
  	  <input type="hidden" name="apalias8"/>
  	  <input type="hidden" name="amalias8"/>
  	  <input type="hidden" name="acalias8"/>
  	  <input type="hidden" name="nalias9"/>
  	  <input type="hidden" name="apalias9"/>
  	  <input type="hidden" name="amalias9"/>
  	  <input type="hidden" name="acalias9"/>
    <? mysql_close(); ?>  
    <table width="750" border="0" cellpadding="0" cellspacing="0">      
      <tr>
        <td><? include('./foot.php');  //Se incluye el banner para el pie de p�gina ?></td>
      </tr>
    </table>  
      
</form>
</body>
</html>