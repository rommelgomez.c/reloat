<html>
<head>
<title>Obtener CURP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="./scripts/Stylesheetpe.css" rel="stylesheet" type="text/css">
<!-- Se incluyen los archivos con los scripts para las m�scaras de los campos de texto -->
<script src="scripts/mask/masks.js"></script>
<script src="scripts/mask/config.js"></script>
<script>
//Valida que se llenen los campos con los datos necesarios
function validar()
{	var campos_validados = true;
	form = document.form1;

		

	if(form.Nombre.value == "")
	{	alert("Ingrese el Nombre");
		campos_validados = false;
		form.Nombre.focus();
	}
	else if(form.apPaterno.value == "")
	{	alert("Ingrese el Apellido Paterno");
		campos_validados = false;
		form.apPaterno.focus();
	}
	/*else if(form.apMaterno.value == "")
	{	alert("Debe escribir el Apellido Materno");
		campos_validados = false;
	}*/
	else if(form.anio.value == "")
	{	alert("Ingrese el A�o de Nacimiento");
		campos_validados = false;
		form.anio.focus();
	}
	else if(form.anio.value.length != 4 || form.anio.value < 1850 || form.anio.value =="0000")
	{	alert("Ingrese el A�o de Nacimiento Correctamente");
		campos_validados = false;
		form.anio.focus();
	}
	else if(form.mes.value == 0)
	{	alert("Seleccione el Mes de Nacimiento");
		campos_validados = false;
		form.mes.focus();
	}
	else if(form.dia.value == 0)
	{	alert("Seleccione el D�a de Nacimiento");
		campos_validados = false;
		form.dia.focus();		
	}
	else if(form.sexo.value == 0) 
	{	alert("Seleccione el Tipo de Sexo");
		campos_validados = false;
		form.sexo.focus();
	}
	else if(form.entidad.value == "") 
	{	alert("Seleccione una Entidad de Nacimiento");
		campos_validados = false;
		form.entidad.focus();
	}		
	
	if(campos_validados)
	{	//Guarda el valor de los campos para mandarlos a la p�gina que busca el curp
		var Nombre = form.Nombre.value;
		var apPaterno = form.apPaterno.value;
		var apMaterno = form.apMaterno.value;
		var anio = form.anio.value;
		var mes = form.mes.value;
		var dia = form.dia.value;
		var sexo = form.sexo.value;
		var entidad = form.entidad.value;
		var band = form.band.value;	
		
		
		opener.document.form1.Nombre.value = Nombre;
		opener.document.form1.apPaterno.value = apPaterno;
		opener.document.form1.apMaterno.value = apMaterno;
		
		if(sexo =='H'){
			opener.document.form1.sexo.value = "MASCULINO";
		}else if(sexo =='M'){
			opener.document.form1.sexo.value = "FEMENINO";
		}else{
			opener.document.form1.sexo.value = sexo;
		}
		
		//opener.document.form1.lugarNacimiento.value = entidad;

		if(band=="A"){
			opener.document.form1.mes.value = mes;
			opener.document.form1.dia.value = dia;
			opener.document.form1.anio.value = anio;
		}else{
			var fechaNac2 = anio+"-"+mes+"-"+dia;
			opener.document.form1.fechaNacimiento.value = fechaNac2;
			opener.document.form1.mesh.value = mes;
			opener.document.form1.diah.value = dia;
			opener.document.form1.anioh.value = anio;
		}	

		
		apPaterno = escape(apPaterno);
		apMaterno = escape(apMaterno);
		Nombre = escape(Nombre);
		
		//Abre una ventana con el curp solicitado 
		window.open("http://consultas.curp.gob.mx/CurpSP/curp1.do?strPrimerApellido="+apPaterno+"&strSegundoAplido="+apMaterno+"&strNombre="+Nombre+"&strdia="+dia+"&strmes="+mes+"&stranio="+anio+"&sSexoA="+sexo+"&sEntidadA="+entidad+"&rdbBD=myoracle&strTipo=A&entfija=DF&depfija=04", '_blank', 'width=417, height=670, location=no');
		self.close();
 	
	}
}

//Funci�n Mayusculas: Convierte a mayusculas lo que se escriba en los campos de texto
function Mayusculas(nombre)
{	var alta= new String();
    alta=nombre.value;
	alta= alta.replace(/(^\s*)|(\s*$)/g,""); 	
    nombre.value=alta.toUpperCase();
}
</script>
</head>
<body leftmargin="0" topmargin="0" bgcolor="#E8F1F8">
<form name="form1" method="post" action="">
<table width="417" height="169" border="0" bgcolor="#E8E8E8">
  <tr bgcolor="#663300"> 
    <td colspan="3" bgcolor="#3983C5"><font color="#FFFFFF">OBTEN TU CURP</font></td>
  </tr>
  <tr bgcolor="#BEB094"> 
    <td colspan="3" bgcolor="#BAD2EA">DATOS</td>
  </tr>  
  <tr>
    <td width="139"><font size="2" color="#FF0000">* </font>Nombre(s)</td>
    <td width="268"><input name="Nombre" value="<?=$nombre?>" type="text" maxlength="40" onChange="Mayusculas(this)" size="40" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"></td>   
  </tr>
  <tr>
    <td><font size="2" color="#FF0000">* </font>Apellido Paterno</td>
    <td><input name="apPaterno" value="<?=$apPat?>" type="text" maxlength="40" onChange="Mayusculas(this)" size="40" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"></td>
  </tr>
  <tr>
    <td><font size="2" color="#006600">* </font>Apellido Materno</td>
    <td><input name="apMaterno" value="<?=$apMat?>" type="text" maxlength="40" onChange="Mayusculas(this)" size="40" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"></td>
  </tr>
  <tr>  </tr>
  <tr>
    <td><font size="2" color="#FF0000">* </font>Fecha de Nacimiento</td>
      <td><font size="2" face="Arial, Helvetica, sans-serif">
      <input name="anio" type="text" id="anio" value="<?=$anio?>" size="5" maxlength="4" OnKeyPress="if(event.keyCode < 48 || event.keyCode > 57 || event.keyCode == 192 || event.keyCode == 16) {event.returnValue = false;}">
      <select name="mes">
      <? 	//Creamos el select de meses

					
	  		$meslabel = array();
	  		$meslabel["0"]="Mes"; $meslabel["1"]="Enero";  $meslabel["2"]="Febrero";  $meslabel["3"]="Marzo";
			$meslabel["4"]="Abril";  $meslabel["5"]="Mayo";  $meslabel["6"]="Junio";  $meslabel["7"]="Julio";
			$meslabel["8"]="Agosto";  $meslabel["9"]="Septiembre";  $meslabel["10"]="Octubre";  $meslabel["11"]="Noviembre";
			$meslabel["12"]="Diciembre";		
		
		 for ($j=0; $j<=12; $j++)
		 {	if ($j>0 && $j<10)
				$k = "0".$j;
            else
               	$k = $j;
				
			if ($mes==$j)
			{ ?><option value="<?=$k;?>" selected><?=$meslabel[$j];?></option><? }
			if ($mes!=$j)
			{ ?><option value="<?=$k;?>"><?=$meslabel[$j];?></option><? }
		 } ?>					
		</select>	       
        <select name="dia">        
        <? //Creamos el select de los dias
			if ($dia==0)?>
        	<option value="0">Dia</option>
		<? for ($j=1; $j<=31; $j++)
		   {	if ($j>0 && $j<10)
					$k = "0".$j;
                else
                   	$k = $j;
				
				if ($dia==$j)
				{ ?><option value="<?=$k;?>" selected><?=$dia;?></option><? }
			 	if ($dia!=$j)
				{ ?><option value="<?=$k;?>"><?=$k;?></option><? }
		   } ?>						  
        </select></FONT></td>
  </tr>
  <tr>
    <td><font size="2" color="#FF0000">* </font>Sexo</td>
      <td><font size="2" face="Arial, Helvetica, sans-serif"></FONT>
      <select name="sexo">
		
      <?  //Creamos el select de sexo
	  	$label = array();
	  		$label["0"]="- Seleccione -";  $label["1"]="MASCULINO";  $label["2"]="FEMENINO";
		$valor = array();
	  		$valor["0"]="";  $valor["1"]="H";  $valor["2"]="M";						
		
		 for ($j=0; $j<=2; $j++)
		 {	if ($sexo==$j)
			{ ?><option value="<?=$valor[$j];?>" selected><?=$label[$j];?></option><? }
			if ($sexo!=$j)
			{ ?><option value="<?=$valor[$j];?>"><?=$label[$j];?></option><? }
		 } ?>					
		</select>      </td>
  </tr>    
  <tr>
    <td><font size="2" color="#FF0000">* </font>Entidad de Nacimiento</td>
    <td><select name="entidad" size="1" id="entidad">
<option selected="" value="">- Seleccione -</option>
<option value="AS">AGUASCALIENTES </option>
<option value="BC">BAJA CALIFORNIA NTE. </option>
<option value="BS">BAJA CALIFORNIA SUR </option>
<option value="CC">CAMPECHE </option>
<option value="CL">COAHUILA </option>
<option value="CM">COLIMA </option>
<option value="CS">CHIAPAS </option>
<option value="CH">CHIHUAHUA </option>
<option value="DF">DISTRITO FEDERAL </option>
<option value="DG">DURANGO </option>
<option value="GT">GUANAJUATO </option>
<option value="GR">GUERRERO </option>
<option value="HG">HIDALGO </option>
<option value="JC">JALISCO </option>
<option value="MC">MEXICO </option>
<option value="MN">MICHOACAN </option>
<option value="MS">MORELOS </option>
<option value="NT">NAYARIT </option>
<option value="NL">NUEVO LEON </option>
<option value="OC">OAXACA </option>
<option value="PL">PUEBLA </option>
<option value="QT">QUERETARO </option>
<option value="QR">QUINTANA ROO </option>
<option value="SP">SAN LUIS POTOSI </option>
<option value="SL">SINALOA </option>
<option value="SR">SONORA </option>
<option value="TC">TABASCO </option>
<option value="TS">TAMAULIPAS </option>
<option value="TL">TLAXCALA </option>
<option value="VZ">VERACRUZ </option>
<option value="YN">YUCATAN </option>
<option value="ZS">ZACATECAS </option>
</select></td>
  </tr>
  <tr>
  <td colspan="2"><br><center>
	<input type="hidden" name="band" value="<?=$band?>"/>
    <input type="button" name="bcurp" id="bcurp" value="Enviar" onClick="validar()">
  </center>   
</table>  
</form>
<p>&nbsp;</p>
<? if (isset($bcurp))
   { ?><script language="javascript">self.close();</script> <? } ?>
</body onunload="alert('BUSQUEDA CANCELADA POR EL USUARIO')";>
</html>