<?
/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Formulario para dar de alta los avisos de testamento de las personas con todos los datos requeridos //
/////////////////////////////////////////////////////////////////////////////////////////////////////////

include ("../Config/testalib.php");	//Se incluye el archivo con las diferentes funciones que se utilizan 
include ("../Config/KXFormDBBased.class.php");	//Se incluye el archivo con las clases para la creaci�n de los campos del formulario
   session_start();
	if (session_is_registered('activa'))
	   $activausr = 1;
	else
	   header("Location: ../index.php");
   conecta ("avisos");	//Funcion para conectarse a la base de datos
   //$user = $idUsr;

   $estado = estadouser($idUsr);	//Funcion para obtener el estado al que pertenece el usuario


	    
   $dte = getdate();   //Obtenemos la fecha
   // La separamos en d�a mes y a�o
   $actualdia = str_pad($dte['mday'], 2, "0", STR_PAD_LEFT);
   $actualmes = str_pad($dte['mon'], 2, "0", STR_PAD_LEFT);
   $actualano = $dte['year'];
   
    
   
 
	//OBTENEMOS LAS FECHA Y HORA DEL DIA  ACTUAL
	$yeara= date("Y");
	$montha= date("m");
	$daya=  date("d");
	$horaa= date("H:i");
	

	
   
   
  //  $SQLdatos ="SELECT * FROM user where idUsr = $user";
//	// echo $SQLdatos;
//	$resulto = mysql_query($SQLdatos);
//	$rst = mysql_fetch_row($resulto);
//	//Guarda los resultados de la consulta en variables
//	$idUsr = $rst[0];
//	$tipoDep =  $rst[12];
//	$idNotario = $rst[13];
	//echo "not:".$idNotario;
	$not = "NOTARIA";
	$dep = "DEPENDENCIA";
	//echo $tipoDep;
	//if($tipoDep == $not)
	//{
	//echo "<br>asi es tipo: ".$tipoDep;
//}else {
	//echo "<br>else, es tipo: ".$tipoDep;}

if($tipoDep == $not)
{
	$sqlstr = "SELECT * FROM catnotarios WHERE idNotario ='$idNotario'"; 
//	echo $sqlstr;
	$result = mysql_query($sqlstr);
	$res = mysql_fetch_row($result);
	$idNotario = $res[0];
	$idEdoNot = $res[1];
	$idMunicipio = $res[2];
	$numNot = $res[3];
	$tipoNot = $res[4]; 
	$nombreNot = $res[5];
	$apPNot = $res[6];
	$apMNot = $res[7];

}	

//OBTENEMOS EL IDUSR PADRE DEL USUARIO DEPENDENCIA PARA LAAUTORIDAD COMPETENTE

 $oUsuPadre="SELECT padre, tipo FROM`user` WHERE idUsr =$idUsr";
 $exoUsuPadre=mysql_query($oUsuPadre) or die($oUsuPadre);
 $res1 = mysql_fetch_row($exoUsuPadre);
 $idUsrPadre=$res1[0];
 $tipoUsu=$res1[1];
 

//OBTENEMOS LA DEPENDENCIA PARA LAAUTORIDAD COMPETENTE

	$oDep="SELECT dependencia FROM`user` WHERE idUsr =$idUsrPadre";
	$exoDep=mysql_query($oDep) or die($oDep);
	$res2 = mysql_fetch_row($exoDep);
	$dependenciaPadre= $res2[0];
	
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- Se incluyen los archivos para las m�scaras de los campos de texto -->
<script src="scripts/mask/masks.js"></script>
<script src="scripts/mask/config.js"></script>
<title>Altas - Registro Nacional de Testamentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="./scripts/Stylesheetpe.css" rel="stylesheet" type="text/css">
<script>
//Asiganaci�n de valores a variables
hide = 1;
NS4 = (document.layers) ? 1 : 0;
IE4 = (document.all) ? 1 : 0;
ver4 = (NS4 || IE4) ? 1 : 0;

adia = <?=$actualdia?>;
ames = <?=$actualmes?>;
aanio = <?=$actualano?>
//Funci�n Validar: Verifica que por lo menos esten los campos de nombre y alguno de los apellidos
function validar(tipob) 
{	
	
		if(tipob=='DEPENDENCIA'){
		
			document.form1.dia2.value=document.form1.dia2b.value;
			document.form1.mes2.value=document.form1.mes2b.value;
			document.form1.anio2.value=document.form1.anio2b.value;
			document.form1.horaRegistro.value=document.form1.horaRegistrob.value;
			
					
		}
		
		//VALIDAMOS LOS DATOS DEL FORMULARIO QUE DEBEN DE SER OBLIGARIOS
		
		if(document.form1.Nombre.value==""){
			alert("Ingrese el Nombre del Testador");
			document.form1.Nombre.focus();
			return '0';
		}
		if(document.form1.apPaterno.value=="" && document.form1.apMaterno.value==""){
			alert("Debe de Ingresar m�nimo un Apellido del Testador");
			document.form1.apPaterno.focus();
			return '0';
		}
		if(document.form1.tipoTestamento.value==""){
			alert("Ingrese el Tipo de Testamento");
			document.form1.tipoTestamento.focus();
			return '0';
		}
		if(document.form1.tipoTestamento.value=="OTRO" && document.form1.tipoTestamento2.value==""){
			alert("Ingrese el Tipo de Testamento");
			document.form1.tipoTestamento2.focus();
			return '0';
		}		
		if(document.form1.escritura.value=="" && document.form1.tipoTestamento.value!='OL�GRAFO'){
			alert("Ingrese el No. de Escritura");
			document.form1.escritura.focus();
			return '0';
		}
		if((document.form1.anio5.value=="") || (document.form1.mes5.value==0) || (document.form1.dia5.value==0)){
			
			if(document.form1.tipoTestamento.value!='OL�GRAFO'){
				alert("Ingrese la Fecha de Escritura");
			}else{
				alert("Ingrese la Fecha de Dep�sito");
			}
			document.form1.mes5.focus();
			return '0';
		}
		if(document.form1.lugarOtorgamiento.value==""){
			alert("Ingrese el Lugar de Otorgamiento");
			document.form1.lugarOtorgamiento.focus();
			return '0';
		}
		
		
		if(tipob=='DEPENDENCIA'){
		
			juezd= document.form1.juez.value;
			longi2=juezd.length;
			
			
			if(document.form1.numNotaria.value=="" && document.form1.tipoTestamento.value!='OL�GRAFO' && longi2 == 0){
				alert("Ingrese el No. de Notar�a");
				document.form1.numNotaria.focus();
				return '0';
			}
			if(document.form1.nombrenot.value=="" && document.form1.tipoTestamento.value!='OL�GRAFO'){

				if(longi2 == 0){
					alert("Ingrese el Nombre del Notario");
				}else{
					alert("Ingrese el Nombre del Juez");
				}
				
				document.form1.nombrenot.focus();
				return '0';
			}
			if(document.form1.apPatNot.value=="" && document.form1.apMatNot.value=="" && document.form1.tipoTestamento.value!='OL�GRAFO'){
					
				if(longi2 == 0){
					alert("Debe de Ingresar M�nimo un Apellido del Notario");
				}else{
					alert("Debe de Ingresar M�nimo un Apellido del Juez");
				}				
				document.form1.apPatNot.focus();
				return '0';
			}

			if(document.form1.tiponot.value=="OTRO" && document.form1.tiponot2.value=="" && longi2 == 0){
				alert("Ingrese el Tipo de Notario");
				document.form1.tiponot2.focus();
				return '0';
			}
			
			if(document.form1.municipio.value=="" && document.form1.tipoTestamento.value!='OL�GRAFO'){
				
				
				if(longi2 == 0){
					alert("Seleccione el Municipio del Notario");
				}else{
					alert("Seleccione el Municipio del Juez");
				}				
				document.form1.municipio.focus();
				return '0';
			}
			
		}	
		
		
		if(document.form1.idMunicipio.value==""){
				alert("Seleccione el Municipio de la Entidad Federativa");
				document.form1.idMunicipio.focus();
				return '0';
		}
		
		
		
		
		
			

	camponombre = form1.Nombre;
	campoap= form1.apPaterno;
	campoam= form1.apMaterno;
	numCaracteres = form1.curp.value.length;
	var respuesta; 
	respuesta=confirm("�Est� seguro de que desea agregar esta informaci�n?");
    if (respuesta == true)
    {	if (numCaracteres != '' && numCaracteres != 18)
		{	alert("El campo de CURP se encuentra incompleto");
		    return '0';
		}		 
	    if (camponombre.value == '' )
	    {	alert("Es necesario capturar el campo de Nombre");
            return '0';
		}
		else		
		{	
			return Checafe();
		}			 
	 
	 	return '1';
	 }
	 else
	 {	return '0';}			   
}   
//Funci�n Oficinara: Hace efectos de habilitar o desabilitar celdas
function oficinara()
{	 
 
		 
	 if (form1.tipoTestamento.value =="OTRO")
	 {			
		form1.tipoTestamento2.disabled=false;	
		visible('tipoTestamento2','on');
		document.form1.tipoTestamento2.focus();			
	 }
	 else
	 {					
		form1.tipoTestamento2.value="";		
		form1.tipoTestamento2.disabled=true;
		visible('tipoTestamento2','');
		
	 }


	if (form1.tiponot.value =="OTRO")
	{	
		form1.tiponot2.disabled=false;						
		visible('tiponot2','on');
		document.form1.tiponot2.focus();
	}
	else
	{	
		
		form1.tiponot2.disabled=true;		
		form1.tiponot2.value="";		
		visible('tiponot2','');
		
	}

		
	if (form1.ofiRegistro.value == "OTRO")
	 {	
		form1.ofiRegistro2.disabled=false;
		form1.ofiRegistro2.size="20";
		form1.ofiRegistro2.focus();
		visible('ofiRegistro2','on');
		document.form1.ofiRegistro2.focus();
	 }
	 else
	 {	
		form1.ofiRegistro2.disabled=true;				
		form1.ofiRegistro2.value="";
		visible('ofiRegistro2','');
		
	 }

		  
	 if (form1.tipoTestamento.value =="OL�GRAFO")
	 {	
	 	
		label = " <font size='2' color='#FF0000' id='depositoh'>* </font> Fecha y Hora de Dep�sito";
		
			document.getElementById("tipoLabel").innerHTML = label;	

			if(document.form1.tipoDepv2.value == document.form1.Depv2.value){

				document.getElementById('municipioh').style.color="#E8E8E8";
				document.getElementById('nombreNoth').style.color="#E8E8E8";
				document.getElementById('APNoth').style.color="#E8E8E8";
				document.getElementById('AMNoth').style.color="#E8E8E8";
				document.getElementById('notariah').style.color="#E8E8E8"


				document.form1.nombrenot.value="";		
				document.form1.apPatNot.value="";		
				document.form1.apMatNot.value="";					   
				document.form1.numNotaria.value="";		
				document.form1.tiponot.value="";		
				document.form1.tiponot2.value="";		
				document.form1.municipio.value="";		
				document.form1.juez.value="";


				document.getElementById('juez1').style.visibility='visible';
				document.getElementById('juez3').style.visibility = "visible";


				form1.nombrenot.disabled=true;
				form1.apPatNot.disabled=true;
				form1.apMatNot.disabled=true;
				form1.numNotaria.disabled=true;
				form1.tiponot.disabled=true;
				form1.tiponot2.disabled=true;
				form1.municipio.disabled=true;
				form1.juez.disabled=true;

			}
		
		

			document.getElementById('escriturah').style.color="#E8E8E8";
		
		
		
		
				
		
		document.form1.clau1.checked= false;
		document.form1.clau2.checked= false;						
		document.form1.oTomo.value="";		
		document.form1.escritura.value="";		
		
		
			
		form1.clau1.disabled=true;
		form1.clau2.disabled=true;
		form1.oTomo.disabled=true;
		form1.escritura.disabled=true;	
			
				
	  }
	  else
	  {	
	  	
	  
		document.getElementById('escriturah').style.color="#FF0000";		
				
		label = " <font size='2' color='#FF0000'>* </font>Fecha y hora de Escritura";
		document.getElementById("tipoLabel").innerHTML = label;


			if(document.form1.tipoDepv2.value == document.form1.Depv2.value){

				document.getElementById('municipioh').style.color="#FF0000";
				document.getElementById('nombreNoth').style.color="#FF0000";
				document.getElementById('APNoth').style.color="#006600";		
				document.getElementById('AMNoth').style.color="#006600";		
				document.getElementById('notariah').style.color="#FF0000";

				

				form1.nombrenot.disabled=false;
				form1.apPatNot.disabled=false;
				form1.apMatNot.disabled=false;
				form1.numNotaria.disabled=false;
				form1.tiponot.disabled=false;
				form1.tiponot2.disabled=false;
				form1.municipio.disabled=false;
				form1.juez.disabled=false;
				

				document.getElementById('numNotaria').readOnly = false;
		
				valoNumNot = document.form1.numNotaria.value;
				
				if(valoNumNot.length>0){
					document.getElementById('juez1').style.visibility='hidden';

				}else{
					document.getElementById('juez1').style.visibility='visible';
				}

				valoJuezv = document.form1.juez.value;
				if(valoJuezv.length>0){
					document.getElementById('juez3').style.visibility='hidden';

				}else{
					document.getElementById('juez3').style.visibility='visible';
				}
				
				

			}	  	
		
		
		form1.clau1.disabled=false;
		form1.clau2.disabled=false;		
		form1.oTomo.disabled=false;
		form1.escritura.disabled=false;	

		
				
	  }
		  		 
	  
}

//Funci�n Verifica: Checa que los alias esten completos y los agrega a los hidden correspondientes
function verifica()
{	if (document.form1.nalias.value == "") 
   	{  	alert ('No se cuenta con ning�n nombre de "Tambi�n conocido como"!');
     	return false;
   	}
	else
	{	if ((document.form1.apalias.value == "") && (document.form1.amalias.value == ""))
    	{	alert ('No se cuenta con ningun apellido de "Tambi�n conocido como"!');
     		return false;
    	}
		valor = document.form1.nalias.value + " " + document.form1.apalias.value + " " + document.form1.amalias.value  + " " + document.form1.acalias.value;
		if (document.form1.listaa.value == "")
			document.form1.listaa.value = valor;
		else
			document.form1.listaa.value = document.form1.listaa.value + "\n" + valor;

		switch (hide)
		{	<? for ($x=0;$x<=9;$x++)
      		{?>
  				case <? echo($x); ?>:
				noalias<?=$x?>=document.form1.nalias.value;
    			document.form1.nalias<?=$x?>.value=noalias<?=$x?>;
    			document.form1.apalias<?=$x?>.value=document.form1.apalias.value;
   				document.form1.amalias<?=$x?>.value=document.form1.amalias.value;
				document.form1.acalias<?=$x?>.value=document.form1.acalias.value;
				break ;	<? 
			} 	?>
		}
		hide=hide+1;
		document.form1.apalias.value="";
		document.form1.amalias.value="";
		document.form1.nalias.value="";
		document.form1.acalias.value="";
		return false;
	}
}

//Funci�n Visible: Hace que un objeto este visible o invisible a los ojos del usuario
function visible(objeto,on)
{	if (on)
	{  	if (NS4)
		{	document.layers[objeto].visibility = "show";   	}
		else
		{   document.all[objeto].style.visibility = "visible"; 	}
  	}
	else
	{  	if (NS4)
		{	document.layers[objeto].visibility = "hide";   	}
		else
		{   document.all[objeto].style.visibility = "hidden";  	}
  	}
}

//Funci�n Confirmaci�n: Pregunta al usuario si desea pasar a la sigiente pag.
function confirmacion ()
{	var respuesta=confirm("�Est� seguro de que desea agregar esta informaci�n?");
    if (agree)
    	return true;
    else
    	return false;
}

//Funci�n CheckKeys: Permite escribir unicamente numeros en los campos de texto
function CheckKeys()
{	if((event.keyCode >= 65 && event.keyCode <= 122) || (event.keyCode >=1 && event.keyCode <= 47))
	{	event.keyCode=0;	}
}	

//Funci�n Checafe: Verifica que las fechas del formulario no sean mayores a la fecha actual.
function Checafe()
{	if (form1.anio5.value)
  	{	if(aanio < form1.anio5.value)  
    	{	
			if(document.form1.tipoTestamento.value!='OL�GRAFO'){
				alert("La Fecha de Escritura No puede ser mayor a la Fecha Actual");
			}else{
				alert("La Fecha de Dep�sito No puede ser mayor a la Fecha Actual");
			}
			
	  		return false;
	 	}
  		else
	 	{	if(aanio == form1.anio5.value && ames < form1.mes5.value)
			{	
				if(document.form1.tipoTestamento.value!='OL�GRAFO'){
					alert("La Fecha de Escritura No puede ser mayor a la Fecha Actual");
				}else{
					alert("La Fecha de Dep�sito No puede ser mayor a la Fecha Actual");
				}				
				
		 		return false;
			}
	  		else
			{	if(aanio == form1.anio5.value && ames == form1.mes5.value && adia < form1.dia5.value)
		  		{	
					if(document.form1.tipoTestamento.value!='OL�GRAFO'){
						alert("La Fecha de Escritura No puede ser mayor a la Fecha Actual");
					}else{
						alert("La Fecha de Dep�sito No puede ser mayor a la Fecha Actual");
					}
					
	 	   			return false;
	      		}
	   		}
     	}
  	}

			document.form1.dia2.value=document.form1.dia2b.value;
			document.form1.mes2.value=document.form1.mes2b.value;
			document.form1.anio2.value=document.form1.anio2b.value;
			document.form1.horaRegistro.value=document.form1.horaRegistrob.value;

	if (form1.anio2.value)
  	{	if(aanio < form1.anio2.value)  
     	{	alert ("La Fecha de Registro en la Dependencia No puede ser mayor a la Fecha Actual");
	  		return false;
	 	}
 		else
	 	{	if(aanio == form1.anio2.value && ames < form1.mes2.value)
			{	alert ("La Fecha de Registro en la Dependencia No puede ser mayor a la Fecha Actual");
		 		return false;
			}
	  		else
			{	if(aanio == form1.anio2.value && ames == form1.mes2.value && adia < form1.dia2.value)
		  		{	alert ("La Fecha de Registro en la Dependencia No puede ser mayor a la Fecha Actual");
	 	   			return false;
	      		}
	   		}
     	}
  	}

	if (form1.anio.value)
  	{	if(aanio < form1.anio.value)
     	{	alert ("La Fecha de Nacimiento No puede ser mayor a la Fecha Actual");
	  		return false;
	 	}
  		else
	 	{	if(aanio == form1.anio.value && ames < form1.mes.value)
			{	alert ("La Fecha de Nacimiento No puede ser mayor a la Fecha Actual");
		 		return false;
			}
	  		else
			{	if(aanio == form1.anio.value && ames == form1.mes.value && adia < form1.dia.value)
		  		{	alert ("La Fecha de Nacimiento No puede ser mayor a la Fecha Actual");
	 	   			return false;
	      		}
	   		}	
     	}
  	}	

	form1.altaa.disabled=true;
	return true;
}


//Funci�n Mayusculas: Convierte a mayusculas lo que se escriba en los campos de texto
function Mayusculas(nombre)
{	var alta= new String();
    alta=nombre.value;
	alta= alta.replace(/(^\s*)|(\s*$)/g,""); 	
    nombre.value=alta.toUpperCase();
	
}

//Funci�n CURP: Abre una ventana con un formulario para obtener el CURP desde la p�gina de SEGOB
function obtencurp()
{	form=document.form1;
	var nombre = form.Nombre.value;
	var apPat = form.apPaterno.value;
	var apMat = form.apMaterno.value;
	var sexo;
	var	anio = form.anio.value;
	var	mes = form.mes.value;
	var	dia	= form.dia.value;
	var band="A";
	
	if (form.sexo.value == "MASCULINO" || form.sexo.value == "Masculino"){
		sexo = 1;
	}
	if (form.sexo.value == "FEMENINO" || form.sexo.value == "Femenino"){
		sexo = 2;
	}
	
	nombre = escape(nombre);
	apPat = escape(apPat);
	apMat = escape(apMat);
		
	childWindow=open('obtencurp.php?nombre='+nombre+'&apPat='+apPat+'&apMat='+apMat+'&anio='+anio+'&mes='+mes+'&dia='+dia+'&sexo='+sexo+'&band='+band,'window','resizable=no,width=417,height=235');
    if (childWindow.opener == null) childWindow.opener = self;

	//window.open("obtencurp.php?nombre="+nombre+"&apPat="+apPat+"&apMat="+apMat+"&anio="+anio+"&mes="+mes+"&dia="+dia+"&sexo="+sexo, 'window', 'width=417, height=215, location=no'); 
}

function obtencurp2(){

	var opcionesb="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=150, height=310, top=0, left=0";
	
	window.open("http://consultas.curp.gob.mx/","",opcionesb);

}

function seleccionarMes(elemento){
	var combo = document.forms["form1"].mes2b;
   	var cantidad = combo.length;
   	for (i = 0; i < cantidad; i++) {
    	  if (combo[i].value == elemento) {
        	 combo[i].selected = true;
      	}   
   	}

}



function seleccionarDia(elemento){
	var combo = document.forms["form1"].dia2b;
   	var cantidad = combo.length;
   	for (i = 0; i < cantidad; i++) {
    	  if (combo[i].value == elemento) {
        	 combo[i].selected = true;
      	}   
   	}

}

function seleccionarDM(elemento,elemento1,tipo){

	seleccionarDia(elemento);
	seleccionarMes(elemento1);
	
	if(tipo=='DEPENDENCIA'){
		
		document.form1.dia2b.disabled=false;
		document.form1.mes2b.disabled=false;
		document.form1.anio2b.disabled=false;
		document.form1.horaRegistrob.disabled=false;		
		
	}else{
	
		document.form1.dia2b.disabled=true;
		document.form1.mes2b.disabled=true;
		document.form1.anio2b.disabled=true;
		document.form1.horaRegistrob.disabled=true;

		    

		document.getElementById('notariah').style.color="#E8E8E8";
		document.getElementById('municipioh').style.color="#E8E8E8";
		document.getElementById('fechaRegDep3').style.color="#E8E8E8";

	}
}

function cambios(valor){
	 if(valor==1){
 
	 	document.form1.anio.value='';
	 }else{
	 	document.form1.anio5.value='';
	 }
}
//Funci�n Validar: Verifica que los por lo menos esten los campos de nombre y alguno de los apellidos y las 3 tipos de fechas

//FUNCI�N QUE VALIDA LA EDAD

function edados(fecha) {
	
		
	hoy = new Date()
	var array_fecha = fecha.split("-")
	var ano
	ano = parseInt(array_fecha[2], 10);
	
	if (isNaN(ano))
		return false
	var mes
	mes = parseInt(array_fecha[1], 10);
	
	if (isNaN(mes))
		return false
	var dia
	dia = parseInt(array_fecha[0], 10);
	
	if (isNaN(dia))
		return false
	edad = hoy.getFullYear() - ano - 1;
	
	if (hoy.getMonth() + 1 - mes < 0) {
		
		return edad;
		
	
	}
	
	if (hoy.getMonth() + 1 - mes > 0) {
				
		edad = edad + 1
		return edad;
	}
	
	if (hoy.getUTCDate() - dia >= 0) {
		
		edad = edad + 1
		return edad;
	}
		
		 return edad;
}
//FIN FUNCION VALIDA EDAD

//FUNCION QUE VALIDA LA MAYOR DE LAS FECHAS

function compare(fecha, fecha2)  
{  
	var array_fecha = fecha.split("-")
	var array_fecha2 = fecha2.split("-")
	
	var xDay=array_fecha[0];  
	var xMonth=array_fecha[1]; 
	var xYear=array_fecha[2];  
	var yDay=array_fecha2[0];
	var yMonth=array_fecha2[1];  	  
	var yYear=array_fecha2[2];
	
	  
  	if (xYear> yYear){  
        return(true)  
    }  
    else{  
      	if (xYear == yYear){   
        	if (xMonth> yMonth){  
            	return(true)  
        	}else{   
          		if (xMonth == yMonth){  
            		if (xDay> yDay){  
              			return(true);  
					}else{  
        			    return(false);  
					}
          		}else{  
            		return(false);  
				}
        	}  
       }else{  
        	return(false);  
	   }
    }  
}    

//FUNCION VALIDA TIEMPO DE DOS FECHAS

function entre(fecha1,fecha2) {
	hoy = new Date()
	var array_fecha1 = fecha1.split("-")
	var array_fecha2 = fecha2.split("-")
	var ano1
	var ano2
	ano1 = parseInt(array_fecha1[2], 10);
	ano2 = parseInt(array_fecha2[2], 10);
	
	if (isNaN(ano1) || isNaN(ano2) )
		return false
	var mes1
	var mes2
	mes1 = parseInt(array_fecha1[1], 10);
	mes2 = parseInt(array_fecha2[1], 10);
	
	if (isNaN(mes1) || isNaN(mes2))
		return false
	var dia1
	var dia2
	dia1 = parseInt(array_fecha1[0], 10);
	dia2 = parseInt(array_fecha2[0], 10);
	
	
	
	if (isNaN(dia1) || isNaN(dia2))
		return false
	
	edad = ano1 - ano2 - 1;
	
	if (mes1 - mes2 < 0) {
		
		return edad;	
	}
	
	if (mes1 - mes2 > 0) {
				
		edad = edad + 1
		return edad;
	}
	
	if (dia1 - dia2 >= 0) {
		
		edad = edad + 1
		return edad;
	}
	
		 return edad;
}
//FIN FUNCION VALIDA TIEMPO DE DOS FECHAS


//INICIO FUNCION VALIDA LA FECHAS DEL TESTAMENTO
function validardos(){
	var pasa;
	var fecha_actual;
    var aniosFechNac;
	var aniosFescFnac;
	var aniosFregFesc;
	var aniosFregFnac;
	var fechaNac="";
	var fechaNac2="";
	var fechaEsc="";
	var fechaReg="";
	var aniosFechActEsc;
	var aniosFechActReg;


			document.form1.dia2.value=document.form1.dia2b.value;
			document.form1.mes2.value=document.form1.mes2b.value;
			document.form1.anio2.value=document.form1.anio2b.value;
			document.form1.horaRegistro.value=document.form1.horaRegistrob.value;

	
	fechaNac2 = form1.dia.value+"-"+form1.mes.value+"-"+form1.anio.value;
	
	//alert(fechaNac2.length);
	if(fechaNac2.length > 4 && fechaNac2.length !=10){
			alert("Favor de Ingresar la Fecha de Nacimiento Correctamente");
			document.form1.mes.focus();
			return '0';
				
	}

	if(fechaNac2.length > 4 && fechaNac2.length ==10){
	
		if(form1.anio.value=="0000" || form1.anio.value < 1850){
				alert("Favor de Ingresar la Fecha de Nacimiento Correctamente");
				document.form1.mes.focus();
				return '0';
		}
	
	}
	
	fechaEsc2 = form1.dia5.value+"-"+form1.mes5.value+"-"+form1.anio5.value;

	if(fechaEsc2.length !=10){
			
			if(document.form1.tipoTestamento.value!='OL�GRAFO'){
				alert("Favor de Ingresar la Fecha de Escritura Correctamente");	
			}else{
				alert("Favor de Ingresar la Fecha de Dep�sito Correctamente");
			}			
			document.form1.mes5.focus();
			return '0';
				
	}

	if(fechaEsc2.length ==10){
	
		if(form1.anio5.value=="0000" || form1.anio5.value < 1850){
			
			if(document.form1.tipoTestamento.value!='OL�GRAFO'){
				alert("Favor de Ingresar la Fecha de Escritura Correctamente");
			}else{
				alert("Favor de Ingresar la Fecha de Dep�sito Correctamente");
			}
			
			document.form1.mes5.focus();
			return '0';
		}
	
	}

	
	fechaRes2 = form1.dia2b.value+"-"+form1.mes2b.value+"-"+form1.anio2b.value;

	if(fechaRes2.length !=10){
			alert("Favor de Ingresar la Fecha de Registro Correctamente");
			document.form1.mes2b.focus();
			return '0';
				
	}	

	if(fechaRes2.length ==10){
	
		if(form1.anio2b.value=="0000" || form1.anio2b.value < 1850){
				alert("Favor de Ingresar la Fecha de Registro Correctamente");
				document.form1.mes2b.focus();
				return '0';
		}
	
	}
	

	if(form1.dia.value > 0 && form1.mes.value > 0 && form1.anio.value!="" && form1.anio.value > 0){	
	 	fechaNac = form1.dia.value+"-"+form1.mes.value+"-"+form1.anio.value;
	}

	
	
	if(form1.dia5.value > 0 && form1.mes5.value > 0 && form1.anio5.value!="" && form1.anio5.value > 0){
	 fechaEsc = form1.dia5.value+"-"+form1.mes5.value+"-"+form1.anio5.value;
	}	 
	 
	if(form1.dia2.value > 0 && form1.mes2.value > 0 && form1.anio2b.value!="" && form1.anio2b.value > 0){ 
	
	fechaReg = form1.dia2.value+"-"+form1.mes2.value+"-"+form1.anio2b.value;	
	}
		 
		 var fech = new Date();
		 var diaAct1 = fech.getDate();
		 var mesAct1 = fech.getMonth() +1;
		 var anioAct1 = fech.getFullYear();
		 
		 		
		
		   fecha_actual = diaAct1+"-"+mesAct1+"-"+anioAct1;
		
	   		
	  
	
	
		//--INICIO VALIDACI�N DE FECHA NACIMIENTO, FECHA REGISTRO Y FECHA ESCRITURA--//
			if((fechaNac.length >=8) || (fechaEsc.length >=8) || (fechaReg.length >=8)){
				if((fechaNac.length >=8) && (fechaEsc.length >=8) && (fechaReg.length >=8)){
					if(compare(fecha_actual,fechaNac)){ 							
							
						aniosFechNac = edados(fechaNac);
													
						if(aniosFechNac >= 16){
							aniosFechActEsc = entre(fecha_actual,fechaEsc);
							aniosFechActReg = entre(fecha_actual,fechaReg);
							
							
							
							aniosFescFnac = entre(fechaEsc,fechaNac);
							aniosFregFesc = entre(fechaReg,fechaEsc);
							
							if(aniosFechActEsc>= 0 && aniosFechActReg>= 0 && aniosFescFnac >= 16 && aniosFregFesc >= 0){
								
								pasa=validar('<? echo $tipoUsu;?>');
								if(pasa=='1'){
								
									document.form1.submit();
								}
								
								
							}else{
								if(document.form1.tipoTestamento.value!='OL�GRAFO'){
									alert("La Fecha Escritura es mayor a la Fecha Actual o No es mayor a 16 a�os conforme a la Fecha Nacimiento o la Fecha de Registro en la Dependencia es mayor a la Fecha Actual o no es igual o mayor a la Fecha de Escritura. Favor de Verificarlas");	
								}else{
									alert("La Fecha Dep�sito es mayor a la Fecha Actual o No es mayor a 16 a�os conforme a la Fecha Nacimiento o la Fecha de Registro en la Dependencia es mayor a la Fecha Actual o no es igual o mayor a la Fecha de Dep�sito. Favor de Verificarlas");	
								}								
								
								return false;
							
							
							}
							
						}else{
							
								if(document.form1.tipoTestamento.value!='OL�GRAFO'){
									alert("El Testador tiene la Edad de "+aniosFechNac+" a�os y NO tiene la capacidad para otorgar un Testamento o la Fecha de Escritura o la Fecha de Registro en la Dependencia son mayores a la Fecha Actual"); 
								}else{
									alert("El Testador tiene la Edad de "+aniosFechNac+" a�os y NO tiene la capacidad para otorgar un Testamento o la Fecha de Dep�sito o la Fecha de Registro en la Dependencia son mayores a la Fecha Actual"); 
								}
							
								document.form1.mes.focus();
								return false;							
						}		   
						   
						  
					}else{  
						alert("La Fecha de Nacimiento es mayor a la Fecha de Hoy"); 
						document.form1.mes.focus();
						return false;
						
						
					}				
					
				}else if((fechaNac.length < 8) && (fechaEsc.length >= 8) && (fechaReg.length >= 8)){
						
					if(compare(fecha_actual, fechaEsc)){
						 
						 aniosFregFesc = entre(fechaReg,fechaEsc);
							
						if(aniosFregFesc>=0){
							
							pasa=validar('<? echo $tipoUsu;?>');
								if(pasa=='1'){
								
									document.form1.submit();
								}
							
						}else{
								if(document.form1.tipoTestamento.value!='OL�GRAFO'){
									alert("La Fecha de Registro en la Dependencia no es igual o mayor a la Fecha de Escritura. Favor de Verificarla");
								}else{
									alert("La Fecha de Registro en la Dependencia no es igual o mayor a la Fecha de Dep�sito. Favor de Verificarla");
								}
							
								return false;
							
						}
					
					}else{

						if(document.form1.tipoTestamento.value!='OL�GRAFO'){
							alert("La Fecha de Escritura es mayor a la Fecha Actual"); 
						}else{
							alert("La Fecha de Dep�sito es mayor a la Fecha Actual"); 
						}
						
							document.form1.mes5.focus();
							return false;
					}
				
				
			
			}else if((fechaNac.length >= 8) && (fechaEsc.length >= 8) && (fechaReg.length < 8)){
				if(compare(fecha_actual, fechaNac)){
					
					 aniosFechNac =edados(fechaNac);
						
					if(aniosFechNac>=16){
						aniosFescFnac = entre(fechaEsc,fechaNac);	
						aniosFechActEsc = entre(fecha_actual,fechaEsc);
							
						if(aniosFescFnac>=16 && aniosFechActEsc>= 0){
							
							pasa=validar('<? echo $tipoUsu;?>');
								if(pasa=='1'){
								
									document.form1.submit();
								}
								
								
						}else{
							if(document.form1.tipoTestamento.value!='OL�GRAFO'){
								alert("La Fecha de Escritura No es Mayor a 16 a�os conforme a la Fecha Nacimiento o la Fecha de Escritura es Mayor a la Fecha Actual. Favor de Verificarlas");	
							}else{
								alert("La Fecha de Dep�sito No es Mayor a 16 a�os conforme a la Fecha Nacimiento o la Fecha de Dep�sito es Mayor a la Fecha Actual. Favor de Verificarlas");
							}
							
							document.form1.mes5.focus();
							return false;
							
							
						}
						
					}else{
						alert("El Testador tiene la Edad de "+aniosFechNac+" a�os y NO tiene la capacidad para otorgar un Testamento"); 
							document.form1.mes.focus();
							return false;
							
					}
					
				}else{
					alert("La Fecha de Nacimiento es mayor a la Fecha Actual"); 
					document.form1.mes.focus();
					return false;
					
				}
				
				
			}else if((fechaNac.length >= 8) && (fechaEsc.length < 8) && (fechaReg.length >= 8)){
				if(compare(fecha_actual, fechaNac)){
					
					aniosFechNac =edados(fechaNac);
						
					if(aniosFechNac>=16){
						aniosFregFnac = entre(fechaReg,fechaNac);
						
						aniosFechActReg = entre(fecha_actual,fechaReg);
							
						if(aniosFregFnac>=16 && aniosFechActReg>=0){
							
							pasa=validar('<? echo $tipoUsu;?>');
								if(pasa=='1'){
								
									document.form1.submit();
								}
								
								
						}else{
							alert("La Fecha de Registro en la Dependencia No es Mayor a 16 a�os conforme a la Fecha Nacimiento o la Fecha de Registro en la Dependencia es Mayor a la Fecha Actual. Favor de Verificarlas");
							document.form1.mes2b.focus();
							return false;
							
						}
						
					}else{
						alert("El Testador tiene la Edad de "+aniosFechNac+" a�os y NO tiene la capacidad para otorgar un Testamento"); 
							document.form1.mes.focus();
							return false;
							
					}
					
				}else{
					alert("La Fecha de Nacimiento es mayor a la Fecha Actual"); 
					document.form1.mes.focus();
					return false;
					
				}
				
				
			}else{
				pasa=validar('<? echo $tipoUsu;?>');
				if(pasa=='1'){
								
					document.form1.submit();
				}	
			}
		}else{
				pasa=validar('<? echo $tipoUsu;?>');
				if(pasa=='1'){
								
					document.form1.submit();
				}	
		}
		//--FIN VALIDACI�N DE FECHA NACIMIENTO, FECHA REGISTRO Y FECHA ESCRITURA--//

}



//FIN FUNCION VALIDA LA FECHAS DEL TESTAMENTO


//FUNCION QUE OCULTA LAS CAPAS DEPENDIENDO DE UN INPUT
function ocultarjuez(){

	numNotaria = document.form1.numNotaria.value;
	longi=numNotaria.length;
	
	
	
	if(longi>0){

		if(numNotaria < 1){
			alert("El No. de Notar�a no es v�lido, favor de ingresarlo correctamente");
			document.form1.numNotaria.value="";
			 document.form1.numNotaria.focus();
		}else{
			document.getElementById('juez1').style.visibility = "hidden";
			document.form1.juez.value="";
		}
		
	}else{
		document.getElementById('juez1').style.visibility = "visible";
	}
		
}

//FIN FUNCION QUE OCULTA LAS CAPAS DEPENDIENDO DE UN INPUT

//FUNCION QUE PONE EN CERO CUANDO UNGRESAN DATOS EN OTRO CAMPO
function ponercero(){

	jueza= document.form1.juez.value;
	longi=jueza.length;
	

	
	if(longi>0){
		/*document.form1.numNotaria.value=0;		
		document.getElementById( 'numNotaria' ).readOnly = true;*/

		document.form1.numNotaria.value="";
		document.form1.tiponot.value="";
		document.form1.tiponot2.value="";
		//document.form1.municipio.value="";

		form1.numNotaria.disabled=true;
		form1.tiponot.disabled=true;
		form1.tiponot2.disabled=true;
		//form1.municipio.disabled=true;
		
		document.getElementById('juez3').style.visibility = "hidden";
		

		visible('tiponot2','');
		
		document.getElementById('notariah').style.color="#E8E8E8";
		//document.getElementById('municipioh').style.color="#E8E8E8";
		
		
	}else{
		/*document.form1.numNotaria.value="";
		document.getElementById( 'numNotaria' ).readOnly = false;*/

		form1.numNotaria.disabled=false;
		form1.tiponot.disabled=false;
		form1.tiponot2.disabled=false;
		//form1.municipio.disabled=false;
		
		document.getElementById('juez3').style.visibility = "visible";
		document.getElementById('notariah').style.color="#FF0000";
		//document.getElementById('municipioh').style.color="#FF0000";
		
	}
		
}

//FIN FUNCION QUE PONE EN CERO CUANDO UNGRESAN DATOS EN OTRO CAMPO

//FUNCION QUE NO DEJA PONER EL FOCO SI TIENE UN CAMPO LLENO

function quitarfoco(){
	juezf= document.form1.juez.value;
	longi=juezf.length;
	
	if(longi>0){
		alert("S� cuenta con un Juez no puede ingresar el No. de Notar�a");
		document.form1.juez.focus();
		
	}else{
		
		document.form1.numNotaria.focus();
	}

}
//FIN FUNCION QUE NO DEJA PONER EL FOCO SI TIENE UN CAMPO LLENO
</script>
<style type="text/css"><!--
.Estilo1 {color: #666666}
--></style>
</head>



<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="seleccionarDM('<? echo $daya;?>','<? echo $montha;?>','<? echo $tipoUsu?>')">
<form name="form1" method="post"  action="./altaedo.php">
<?  include('./head.php'); 
$fdb = new KXFormDBBased("testamentos");	//Creamos el objeto fdb para la tabla de testamentos	






//ARREGLOS PARA LLENAR LOS SELECTS
$genero = array();
	$genero["0"]["label"] = "MASCULINO"; $genero["0"]["value"] = "MASCULINO";
	$genero["1"]["label"] = "FEMENINO"; $genero["1"]["value"] = "FEMENINO";
	
$ecivil = array();
	$ecivil["0"]["label"] = "SOLTERO";							$ecivil["0"]["value"] = "SOLTERO";
	$ecivil["1"]["label"] = "CASADO";							$ecivil["1"]["value"] = "CASADO";
	$ecivil["2"]["label"] = "DIVORCIADO";						$ecivil["2"]["value"] = "DIVORCIADO";
	$ecivil["3"]["label"] = "VIUDO";							$ecivil["3"]["value"] = "VIUDO";
	$ecivil["4"]["label"] = "UNION LIBRE";						$ecivil["4"]["value"] = "UNION LIBRE";
	$ecivil["5"]["label"] = "SOC. DE CONVIVENCIA U HOM�LOGA";	$ecivil["5"]["value"] = "SOC. DE CONVIVENCIA U HOMOLOGA";
	
$tipo = array();
	$tipo["0"]["label"] = "P�BLICO ABIERTO";		$tipo["0"]["value"] = "P�BLICO ABIERTO";
	$tipo["1"]["label"] = "P�BLICO CERRADO";		$tipo["1"]["value"] = "P�BLICO CERRADO";
	$tipo["2"]["label"] = "P�BLICO SIMPLIFICADO";	$tipo["2"]["value"] = "P�BLICO SIMPLIFICADO";

	if($tipoDep !="NOTARIA"){
		$tipo["3"]["label"] = "OL�GRAFO";				$tipo["3"]["value"] = "OL�GRAFO";
		$tipo["4"]["label"] = "OTRO";					$tipo["4"]["value"] = "OTRO";
	}else{
		$tipo["3"]["label"] = "OTRO";					$tipo["3"]["value"] = "OTRO";
	}

	
	
$notario = array();
	$notario["0"]["label"] = "TITULAR";			$notario["0"]["value"] = "TITULAR";
	$notario["1"]["label"] = "SUPLENTE";		$notario["1"]["value"] = "SUPLENTE";
	$notario["2"]["label"] = "ASOCIADO";		$notario["2"]["value"] = "ASOCIADO";
	$notario["3"]["label"] = "PROVISIONAL";		$notario["3"]["value"] = "PROVISIONAL";
	$notario["4"]["label"] = "ADJUNTO";			$notario["4"]["value"] = "ADJUNTO";
	$notario["5"]["label"] = "SUSTITUTO";		$notario["5"]["value"] = "SUSTITUTO";
	$notario["6"]["label"] = "OTRO";			$notario["6"]["value"] = "OTRO";
	
$registro = array();
	$registro["0"]["label"] = $dependencia;							$registro["0"]["value"] = $dependencia;
	$registro["1"]["label"] = "ARCHIVO GENERAL DE NOTARIAS";		$registro["1"]["value"] = "ARCHIVO GENERAL DE NOTARIAS";
	$registro["2"]["label"] = "REGISTRO PUBLICO DE LA PROPIEDAD";	$registro["2"]["value"] = "REGISTRO PUBLICO DE LA PROPIEDAD";
	$registro["3"]["label"] = "SECRETARIA GENERAL DE GOBIERNO";		$registro["3"]["value"] = "SECRETARIA GENERAL DE GOBIERNO";
	$registro["4"]["label"] = "OTRO";								$registro["4"]["value"] = "OTRO";



$regimen = array ();
	$regimen["0"]["label"] = "valor1";			$regimen["0"]["value"] = "valor1";
	$regimen["1"]["label"] = "valor2";			$regimen["1"]["value"] = "valor2";
	
		
// GENERA CAMPOS DEL FORMULARIO
$nombre = $fdb->addTHPInput("Nombre","","text","",40,'tabindex="1" onChange="Mayusculas(this)"  onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$nalias = $fdb->addTHPInput("nalias","","text","","",'tabindex="5" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"');
$apalias = $fdb->addTHPInput("apalias","","text","","",'tabindex="5" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"');
$amalias = $fdb->addTHPInput("amalias","","text","","",'tabindex="5" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"');
$acalias = $fdb->addTHPInput("acalias","","text","","",'tabindex="5" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"');
$listaa = $fdb->addTextArea("listaa","","",'rows="10" wrap="OFF" readonly tabindex="6"');
$apPaterno = $fdb->addTHPInput("apPaterno","","text","",40,'tabindex="2" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$apMaterno = $fdb->addTHPInput("apMaterno","","text","",40,'tabindex="3" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$apConyuge = $fdb->addTHPInput("apConyuge","","text","",40,'tabindex="4" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$lugarNacimiento = $fdb->addTHPInput("lugarNacimiento","","text","",50,'tabindex="6" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode ==34 || event.keyCode ==39 || event.keyCode ==45 || event.keyCode ==92){event.returnValue = false;}"',40);
$anio = $fdb->addTHPInput("anio","","text","",4,'tabindex="8"  OnKeyPress="if (event.keyCode < 48 || event.keyCode > 57 || event.keyCode == 192 || event.keyCode == 16) {event.returnValue = false;}" onFocus="cambios(1)"',6);
$edoCivil = $fdb->addSelectList("edoCivil",$ecivil,"",array(),'tabindex = "11"');
$sexo = $fdb->addSelectList("sexo",$genero,"",array(),'tabindex="7"');
$curp = $fdb->addTHPInput("curp","","text","",18,'tabindex="11" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=48 && event.keyCode <= 57){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = false;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else{event.returnValue = false;}"',40);
$nacionalidad = $fdb->addTHPInput("nacionalidad","","text","",40,'tabindex="5" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else{event.returnValue = false;}" ',"");
$domicilio = $fdb->addTextArea("domicilio","","",'rows="3" wrap="OFF" tabindex="12" cols="38" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode ==34 || event.keyCode ==39 || event.keyCode ==45 || event.keyCode ==92){event.returnValue = false;}"');
$nomPadre = $fdb->addTHPInput("nomPadre","","text","",60,'tabindex="12" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$apPatPadre = $fdb->addTHPInput("apPatPadre","","text","",30,'tabindex="13" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$apMatPadre = $fdb->addTHPInput("apMatPadre","","text","",30,'tabindex="14" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$nomMadre = $fdb->addTHPInput("nomMadre","","text","",60,'tabindex="15" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$apPatMadre = $fdb->addTHPInput("apPatMadre","","text","",40,'tabindex="16" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$apMatMadre = $fdb->addTHPInput("apMatMadre","","text","",40,'tabindex="17" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$apConMadre = $fdb->addTHPInput("apConMadre","","text","","",'tabindex="18" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',40);
$tipoTestamento = $fdb->addSelectList("tipoTestamento",$tipo,"",array(),'onChange="javascript:oficinara();" tabindex = "19"');
$escritura = $fdb->addTHPInput("escritura","","text","","",'tabindex="21" id="escritura" onChange="Mayusculas(this)" OnKeyPress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" onChange="Mayusculas(this)"',20);
$oTomo = $fdb->addTHPInput("oTomo","","text","","",'tabindex="22" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=48 && event.keyCode <= 57){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = false;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else{event.returnValue = false;}"');
$anio5 = $fdb->addTHPInput("anio5","","text","",4,'tabindex="24" OnKeyPress="if(event.keyCode < 48 || event.keyCode > 57 || event.keyCode == 192 || event.keyCode == 16) {event.returnValue = false;}" onFocus="cambios(5)"',6);
$horaInstrumento = $fdb->addTHPInput("horaInstrumento","","text","",5,'tabindex="27" onKeyUp="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" onfocus="timeMask2(this)"',6);
$nombrenot = $fdb->addTHPInput("nombrenot","","text","","",'tabindex="30" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
$apPatNot = $fdb->addTHPInput("apPatNot","","text","","",'tabindex="31" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
$apMatNot = $fdb->addTHPInput("apMatNot","","text","","",'tabindex="32" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',30);
$numNotaria = $fdb->addTHPInput("numNotaria","","text","","",'tabindex = "29" id="numNotaria" OnKeyPress="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" onKeyUp="javascript:ocultarjuez();" OnFocus="javascript:quitarfoco();"',20);
$tiponot = $fdb->addSelectList("tiponot",$notario,"",array(),'onChange="javascript:oficinara();" tabindex="29"');
$juez = $fdb->addTHPInput("juez","","text","",100,'tabindex="36" onChange="Mayusculas(this)" onKeyUp="javascript:ponercero();" onKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=48 && event.keyCode <= 57){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = true;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==46  || event.keyCode ==38 || event.keyCode ==196 || event.keyCode ==203 || event.keyCode ==207 || event.keyCode ==214 || event.keyCode ==220 || event.keyCode ==255 || event.keyCode ==228 || event.keyCode ==235 || event.keyCode ==239 || event.keyCode ==246 || event.keyCode ==252){event.returnValue = true;} else{event.returnValue = false;}"',70);
$procNumIntEdo = $fdb->addTHPInput("procNumIntEdo","","text","","",'tabindex="37" OnKeyPress="if(event.keyCode >=65 && event.keyCode <= 90){event.returnValue = true;} else if(event.keyCode >=48 && event.keyCode <= 57){event.returnValue = true;} else if(event.keyCode >=97 && event.keyCode <=122){event.returnValue = true;}else if(event.keyCode ==32){event.returnValue = false;} else if(event.keyCode ==209){event.returnValue = true;} else if(event.keyCode ==241){event.returnValue = true;} else if(event.keyCode ==47 || event.keyCode ==95){event.returnValue = true;} else{event.returnValue = false;}"');
$anio2b = $fdb->addTHPInput("anio2b",$yeara,"text","",4,'tabindex="36" OnKeyPress="if (event.keyCode < 48 || event.keyCode > 57 || event.keyCode == 192 || event.keyCode == 16) {event.returnValue = false;}"',6);
$horaRegistrob = $fdb->addTHPInput("horaRegistrob","","text","",5,'tabindex="36" onKeyUp="if (event.keyCode < 48 || event.keyCode > 57) event.returnValue = false;" onfocus="timeMask2(this)"',6);	
//$ofiRegistro = $fdb->addSelectList("ofiRegistro",$dependenciaPadre,"text","","",'tabindex="42" onChange="Mayusculas(this)"',30);
$notas = $fdb->addTextArea("notas","","",'rows="5" cols="50" tabindex="44" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode ==34 || event.keyCode ==39 || event.keyCode ==45 || event.keyCode ==92){event.returnValue = false;}"');
?><br>
<table width="750" border="0" bgcolor="#E8E8E8">
  <tr bgcolor="#663300"> 
    <td colspan="3" bgcolor="#3983C5"><font color="#FFFFFF">ALTA DE AVISO DE TESTAMENTO</font></td>
  </tr>
  <tr bgcolor="#BEB094"> 
    <td colspan="3" bgcolor="#BAD2EA">DATOS DEL TESTADOR</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
    <td width="366"><input name="alias" type="button" value="Tambi&eacute;n conocido como:" onClick="javascript:visible('talias',true);" style="background-color:#333333; color:#FFFFFF; font-weight:bold;"/></td>
  </tr>
  
  <tr>
    <td width="145"><font size="2" color="#FF0000">* </font>Nombre(s)</td>
    <td width="225"><?=$nombre;?></td>
    <td rowspan="10"><font size="1" color="#FF0000">* Informaci�n Obligatoria</font>
	<div id="talias" style="visibility:hidden; border:double; border-width:1px; top: 203px; left: 458px;"> 
	<table width="96%" height="182" border="0" >
        <tr> 
          <td width="20%" height="24">Nombre(s)</td>
          <td width="32%"><?=$nalias;?></td>
          <td width="48%" rowspan="5"><?=$listaa;?></td>
        </tr>
        <tr> 
          <td height="34">Apellido Paterno</td>
          <td height="34"><?=$apalias;?></td>
        </tr>
        <tr> 
          <td height="34">Apellido Materno</td>
          <td height="34"><?=$amalias;?></td>
        </tr>
        <tr> 
          <td height="34">Apellido del C�nyuge</td>
          <td height="34"><?=$acalias;?></td>
        </tr>		
        <tr> 
          <td height="31" colspan="2"><input name="add"  type="button" value="Agregar" onClick="return verifica();" tabindex="51"/></td>
        </tr>
      </table></div></td>
  </tr>
  <tr>
    <td><font size="2" color="#006600">* </font>Apellido Paterno</td>
    <td><?=$apPaterno;?></td>
  </tr>
  <tr>
    <td><font size="2" color="#006600">* </font>Apellido Materno</td>
    <td><?=$apMaterno;?></td>
  </tr>
  <tr>
    <td>Apellido del C&oacute;nyuge</td>
    <td><?=$apConyuge;?></td>
  </tr> 
  <tr>
    <td>Nacionalidad</td>
    <td><?=$nacionalidad;?></td>
  </tr>
  <tr>
    <td>Lugar de Nacimiento</td>
    <td><?=$lugarNacimiento;?></td>
  </tr>
  <tr>
    <td>Sexo</td>
    <td><?=$sexo;?></td>
    </tr>
  <tr>
    <td>Fecha de Nacimiento</td>
      <td><font size="3" face="Arial, Helvetica, sans-serif"><?=$anio;?>

	 <input type="hidden" name="tipoDepv2" value="<? echo $tipoDep;?>"/>
	 <input type="hidden" name="Depv2" value="<? echo $dep;?>"/>

        <select name="mes" tabindex = "9">
          <option value="0" selected>Mes</OPTION>
          <option value="01">Enero</OPTION>
          <option value="02">Febrero</OPTION>
          <option value="03">Marzo</OPTION>
          <option value="04">Abril</OPTION>
          <option value="05">Mayo</OPTION>
          <option value="06">Junio</OPTION>
          <option value="07">Julio</OPTION>
          <option value="08">Agosto</OPTION>
          <option value="09">Septiembre</OPTION>
          <option value="10">Octubre</OPTION>
          <option value="11">Noviembre</OPTION>
          <option value="12">Diciembre</OPTION>
        </SELECT>
        <select name="dia" tabindex = "10">
          <option value="0" selected>Dia</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
        </select>
      </FONT></td>
  </tr>	
  <tr>
    <td>Estado Civil</td>
    <td><?=$edoCivil;?></td>
  </tr>
  <tr>
    <td>CURP</td>
    <td><?=$curp;?> &nbsp; <!--<a href="obtencurp.php" onClick="obtencurp();return false" target="_blank">--><a href="#" onClick="obtencurp2()">Obt�n su CURP</a></td>
  </tr>
  <tr>
    <td>Domicilio</td>
    <td><?=$domicilio;?></td>
  </tr>
   </table>
<table width="750" border="0" bgcolor="#E8E8E8">
  <tr bgcolor="#BEB094"> 
    <td colspan="5" bgcolor="#BAD2EA">DATOS DE LOS PADRES</td>
  </tr>
  <tr> 
    <td colspan="2">PADRE</td>
    <td width="37">&nbsp;</td>
    <td colspan="2">MADRE</td>
  </tr>
  <tr>
    <td width="141">Nombre(s)</td>
    <td width="199"><font size="3" face="Arial, Helvetica, sans-serif"><?=$nomPadre;?></FONT></td>
    <td width="37">&nbsp;</td>
    <td width="132">Nombre(s)</td>
    <td width="219"><font size="3" face="Arial, Helvetica, sans-serif"><?=$nomMadre;?></FONT></td>
  </tr>
  <tr>
    <td>Apellido Paterno</td>
    <td><font size="3" face="Arial, Helvetica, sans-serif"><?=$apPatPadre;?></FONT></td>
    <td>&nbsp;</td>
    <td>Apellido Paterno</td>
    <td><font size="3" face="Arial, Helvetica, sans-serif"><?=$apPatMadre;?></FONT></td>
  </tr>
  <tr>
    <td>Apellido Materno</td>
    <td><font size="3" face="Arial, Helvetica, sans-serif"><?=$apMatPadre;?></FONT></td>
    <td>&nbsp;</td>
    <td>Apellido Materno</td>
    <td><font size="3" face="Arial, Helvetica, sans-serif"><?=$apMatMadre;?></FONT></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Apellido del C&oacute;nyuge</td>
    <td><?=$apConMadre;?></td>
  </tr>
</table>
<table width="750" border="0" bgcolor="#E8E8E8">
  <tr bgcolor="#BEB094"> 
    <td colspan="4" bgcolor="#BAD2EA">DATOS DEL INSTRUMENTO</td>
  </tr>
  <tr>
    <td width="141"><font size="2" color="#FF0000">* </font>Tipo de Testamento</td>
    <td width="202"><?=$tipoTestamento;?><br>
        <input type="text" name="tipoTestamento2" size="15" style="visibility:hidden;" disabled tabindex = "20" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode ==34 || event.keyCode ==39 || event.keyCode ==45 || event.keyCode ==92){event.returnValue = false;}"/>
      </td>
    <td width="117"><font size="2" color="#FF0000" id="escriturah">*</font>No. de Escritura</td>
    <td width="278"><?=$escritura;?></td>
  </tr>

  <tr>
    <td>Volumen, Tomo o Libro</td>
    <td><?=$oTomo;?></td>
    <td><span id="tipoLabel"><font size="2" color="#FF0000">* </font>Fecha y Hora de Escritura </span></td>
      <td> <?=$anio5;?>
        <select name="mes5" tabindex = "25">
        <option value="0" selected>Mes</OPTION>
        <option value="01">Enero</OPTION>
        <option value="02">Febrero</OPTION>
        <option value="03">Marzo</OPTION>
        <option value="04">Abril</OPTION>
        <option value="05">Mayo</OPTION>
        <option value="06">Junio</OPTION>
        <option value="07">Julio</OPTION>
        <option value="08">Agosto</OPTION>
        <option value="09">Septiembre</OPTION>
        <option value="10">Octubre</OPTION>
        <option value="11">Noviembre</OPTION>
        <option value="12">Diciembre</OPTION>
      </SELECT>
        <select name="dia5" tabindex = "26">
          <option value="0" selected>Dia</option>
          <option value="01">01</option>
          <option value="02">02</option>
          <option value="03">03</option>
          <option value="04">04</option>
          <option value="05">05</option>
          <option value="06">06</option>
          <option value="07">07</option>
          <option value="08">08</option>
          <option value="09">09</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
        </select>		
		<?=$horaInstrumento;?></td>
  </tr>
  <tr>
    <td colspan="2"><font size="2" color="#FF0000">* </font>Lugar de Otorgamiento (Poblaci&oacute;n o Municipio)</td>
    <td colspan="2"><input name="lugarOtorgamiento" id="lugarOtorgamiento5" size="60" maxlength="100" tabindex="27" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode ==34 || event.keyCode ==39 || event.keyCode ==45 || event.keyCode ==92){event.returnValue = false;}"/>
     </td>
  </tr>
  <tr>
    <td colspan="2">Disposiciones de Contenido Irrevocable</td>
    <td colspan="2">
      SI <input type="radio" name="clausulasIrrevocables" id="clau1" value="SI" tabindex = "28"/>
      NO <input type="radio" name="clausulasIrrevocables" id="clau2" value="NO" tabindex = "29"/>
      </td>
  </tr>
</table>
<table width="750" border="0" bgcolor="#E8E8E8">
  <tr bgcolor="#BEB094"> 
    <td colspan="4" bgcolor="#BAD2EA">DATOS DEL NOTARIO<? if($tipoDep == $dep){?> <? if($estado['idEstado'] == 9){?> / C&Oacute;NSUL<? }?> / JUEZ <? }?> </td>
  </tr>
  <tr>
	<td colspan="4">
		<div id="juez3">
            <table>	
                <tr>
                    <td width="147"><font size="2" color="#FF0000" id="notariah">* </font>No. de Notar&iacute;a </td>
                    <td width="171"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">
                    <? if($tipoDep == $dep) { echo $numNotaria; } else{if(isset($numNot)){ echo $numNot; }}?>
                    </FONT></td>

                    <td width="174">Tipo de Notario</td>
                    <td><? if($tipoDep == $dep) { echo $tiponot; } else{ if(isset($tipoNot)){echo $tipoNot; }}?>
                        <input type="text" name="tiponot2" disabled style="visibility:hidden;" tabindex="29" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode ==34 || event.keyCode ==39 || event.keyCode ==45 || event.keyCode ==92){event.returnValue = false;}"/></td>
                </tr>
            </table>
		</div>
   </td>
	
  </tr>
  <tr>
    <td width="154" rowspan="2"><font size="2" color="#FF0000"> </font>Nombre</td>
    <td width="164"><? if($tipoDep == $dep) { echo $nombrenot; } else{ if(isset($nombreNot)){echo $nombreNot; }}?></td>
    <td width="174"><? if($tipoDep == $dep) { echo $apPatNot; } else{ if(isset($nombreNot)){echo $apPNot; }}?></td>
    <td width="240"><? if($tipoDep == $dep) { echo $apMatNot; } else{ if(isset($nombreNot)){echo $apMNot; }}?></td>
  </tr>


  <tr>
    <td><span class="Estilo1"><? if($tipoDep == $dep) {?> <font size="2" color="#FF0000" id="nombreNoth">* </font>Nombre  <? }?></span></td>
    <td><span class="Estilo1"><? if($tipoDep == $dep) {?> <font size="2" color="#006600" id="APNoth">* </font>Apellido Paterno  <? }?></span></td>
    <td><span class="Estilo1"><? if($tipoDep == $dep) {?> <font size="2" color="#006600" id="AMNoth">* </font>Apellido Materno  <? }?></span></td>
  </tr> 

  <tr>
    <td>Entidad Federativa</td>
    <td><?	//Imprime el estado al que pertenece el notario
    $sqlquery = "SELECT idEstado,Estado from catestado where idEstado = ". $estado['idEstado'];
	$queryresult = mysql_query($sqlquery);
	$datos=mysql_fetch_array($queryresult); 
	echo $datos["Estado"];	?>
      </td>
    <td><font size="2" color="#FF0000" id="municipioh">* </font>Municipio</td> 
    <td>
	<? if($tipoDep == $dep) { ?> 
	<select name="municipio" tabindex="32">
    <?
 		echo "<OPTION selected value=''>Selecciona Municipio</OPTION>\n";	  
		$SQLstr = "SELECT idMunicipio,Municipio FROM catmunicipios where idEstado =". $estado['idEstado'];
        $result = mysql_query($SQLstr);
        do {	$rst = mysql_fetch_row($result);
                if ($rst)
				{	$idCat = $rst[0];
                    $Catego = $rst[1];
                    echo ("<OPTION value=\"$idCat\">$Catego</OPTION>\n");
                }
           } while ($rst);
	 ?>
    </select>
	<? } else {
	if(isset($nombreNot)){
	 $SQL="Select * from catmunicipios where idMunicipio = $idMunicipio";
	  	$result = mysql_query($SQL);
	     $res = mysql_fetch_row($result);
		 echo $res[1];
	 }}?>
	</td>
  </tr>
  	<? if($tipoDep!=$not){?>
  <tr>
  	<td colspan="4">
    <div id="juez1">
  		<table>
          <tr>
            <td>Adscripci�n del Juez <? if($estado['idEstado'] == 9){?>/ Circunscripci&oacute;n del C&oacute;nsul <? }?> (en su caso)</td>
            <td colspan="2"><?=$juez;?></td>
            <td>&nbsp;</td>
          </tr>
        </table>
	</div>  
    </td>
   </tr>
   <? }?>
  
   
</table>
<table width="750" border="0" bgcolor="#E8E8E8">
  <tr bgcolor="#BEB094"> 
    <td colspan="4" bgcolor="#BAD2EA">DATOS DE REGISTRO DEL AVISO EN LA ENTIDAD FEDERATIVA</td>
  </tr>
  <tr>
    <td>Entidad Federativa</td>
    <td><?
    $sqlquery = "SELECT idEstado,Estado from catestado where idEstado = ".$estado['idEstado'];
	$queryresult = mysql_query($sqlquery);
	$datos=mysql_fetch_array($queryresult); 
	echo $datos["Estado"];
	?>
    </td>
    <td width="96"><font size="1" color="#FF0000" id="fechaRegDep3">* </font>Fecha y Hora de Registro en la Dependencia</td>
    <td width="246"> <font size="3" face="Arial, Helvetica, sans-serif"><?=$anio2b;?></font>
	<font size="3" face="Arial, Helvetica, sans-serif">
       
        <select name="mes2b" tabindex = "36">
          <option value="0" selected>Mes</option>
          <option value="01">Enero</option>
          <option value="02">Febrero</option>
          <option value="03">Marzo</option>
          <option value="04">Abril</option>
          <option value="05">Mayo</option>
          <option value="06">Junio</option>
          <option value="07">Julio</option>
          <option value="08">Agosto</option>
          <option value="09">Septiembre</option>
          <option value="10">Octubre</option>
          <option value="11">Noviembre</option>
          <option value="12">Diciembre</option>
        </select>
        </font>
        <select name="dia2b" tabindex = "36">
        <option value="0" selected>Dia</option>
        <option value="01">01</option>
        <option value="02">02</option>
        <option value="03">03</option>
        <option value="04">04</option>
        <option value="05">05</option>
        <option value="06">06</option>
        <option value="07">07</option>
        <option value="08">08</option>
        <option value="09">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option>
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
      </select><?=$horaRegistrob;?></font>
      <font size="3" face="Arial, Helvetica, sans-serif">&nbsp; </font></td>
  </tr>
  <tr>
    <td>Autoridad Competente</td>
    
    
   <td>
   	<? echo strtoupper($dependenciaPadre);	
	?>
    <input type="hidden" name="ofiRegistro" value="<? echo $dependenciaPadre;?>" onChange="Mayusculas(this)"/>
    
	
	<?/*=$ofiRegistro;*/?>
	  <input type="text" disabled=true name="ofiRegistro2" tabindex="43" style="visibility:hidden;" onChange="Mayusculas(this)" onKeyPress="if(event.keyCode ==34 || event.keyCode ==39 || event.keyCode ==45 || event.keyCode ==92){event.returnValue = false;}"/> 
    </td>
    <td><font size="2" color="#FF0000">* </font>Municipio</td>
    <td><select name="idMunicipio" tabindex = "36">
    <?
	/*    if ($estado[idEstado] == 9)
	{	echo "<OPTION value=0>NO DISPONIBLE</OPTION>\n";	}
	else
	{ */
 		echo "<OPTION selected value=''>Selecciona Municipio</OPTION>\n";	  
		$SQLstr = "SELECT idMunicipio,Municipio FROM catmunicipios where idEstado =".$estado[idEstado];
        $result = mysql_query($SQLstr);
        do {	$rst = mysql_fetch_row($result);
                if ($rst)
				{	$idCat = $rst[0];
                    $Catego = $rst[1];
                    echo ("<OPTION value=\"$idCat\">$Catego</OPTION>\n");
                }
           } while ($rst);
	// } ?>
    </select></td>
  </tr>  
  <tr>
      <td height="25">Capturista</td>
    <td><? echo("$nombrec"); ?>
      </td>
    <td>No. Control de la Dependencia</td>
    <td><?=$procNumIntEdo?></td>
  </tr>
</table>
  <table width="750" border="0" align="left" cellpadding="0" cellspacing="0">
  <tr>
  <td colspan="5"></td>
  </tr> 
  <tr> 
    <td width="388" height="22" bgcolor="#E8E8E8">Notas que contiene el Aviso:</td>
    <td colspan="4" bgcolor="#E8E8E8">&nbsp;</td>
    </tr>
  <tr bgcolor="#E8E8E8"> 
      <td bgcolor="#E8E8E8"><font face="Arial, Helvetica, sans-serif"><?=$notas;?></font></td>
    <td width="48">&nbsp;</td>
    <td width="80"><p> </p></td>
    <td width="128"><p><input  type="button" name="altaa" value="ACEPTAR" tabindex = "45" onClick="validardos()"/></p></td>
    <td width="94"><p><input type="button" name="cancelar" value="REGRESAR" tabindex = "46" onClick="document.location='./validacion/menu.php?uid=<?=$idUsr?>'"/></p></td>
  </tr>
  <tr>
    <td colspan="5"><?	include('./foot.php');?></td>
    </tr>
</table>
<br><!-- Campos ocultos para llenarlo con los alias -->
  <input type="hidden" name="alta" value="ok"/>
  <input type="hidden" name="nalias1"/>
  <input type="hidden" name="apalias1"/>
  <input type="hidden" name="amalias1"/>
  <input type="hidden" name="acalias1"/>
  <input type="hidden" name="nalias2"/>
  <input type="hidden" name="apalias2"/>
  <input type="hidden" name="amalias2"/>
  <input type="hidden" name="acalias2"/>
  <input type="hidden" name="nalias3"/>
  <input type="hidden" name="apalias3"/>
  <input type="hidden" name="amalias3"/>
  <input type="hidden" name="acalias3"/>
  <input type="hidden" name="nalias4"/>
  <input type="hidden" name="apalias4"/>
  <input type="hidden" name="amalias4"/>
  <input type="hidden" name="acalias4"/>
  <input type="hidden" name="nalias5"/>
  <input type="hidden" name="apalias5"/>
  <input type="hidden" name="amalias5"/>
  <input type="hidden" name="acalias5"/>
  <input type="hidden" name="nalias6"/>
  <input type="hidden" name="apalias6"/>
  <input type="hidden" name="amalias6"/>
  <input type="hidden" name="acalias6"/>
  <input type="hidden" name="nalias7"/>
  <input type="hidden" name="apalias7"/>
  <input type="hidden" name="amalias7"/>
  <input type="hidden" name="acalias7"/>
  <input type="hidden" name="nalias8"/>
  <input type="hidden" name="apalias8"/>
  <input type="hidden" name="amalias8"/>
  <input type="hidden" name="acalias8"/>
  <input type="hidden" name="nalias9"/>
  <input type="hidden" name="apalias9"/>
  <input type="hidden" name="amalias9"/>
  <input type="hidden" name="acalias9"/>
  <input type="hidden" name="idEstado1" value="<?=$estado['idEstado'];?>"/>
  <input type="hidden" name="idEstado" value="<?=$estado['idEstado'];?>"/>
  <input type="hidden" name="capturista" value="<? echo "$nombrec"; ?>"/>
  <input type="hidden" name="estatus" value="Pendiente"/>
  <input type="hidden" name="idUsr" value="<?=$idUsr;?>"/>
  <input type="hidden" name="nombreNot" value="<?=$nombreNot;?>"/>
   <input type="hidden" name="dia2" value="<? echo $daya; ?>"/>
   <input type="hidden" name="mes2" value="<? echo $montha; ?>"/>
   <input type="hidden" name="anio2" value="<? echo $yeara; ?>"/>
   <input type="hidden" name="horaRegistro" value="<? echo $horaa; ?>"/>
 
  <?
  if($tipoDep == $not)
  {
  ?>
  <input type="hidden" name="nombrenot" value="<?=$nombreNot;?>"/>
  <input type="hidden" name="apPatNot" value="<?=$apPNot;?>"/>
  <input type="hidden" name="apMatNot" value="<?=$apMNot;?>"/>
  <input type="hidden" name="numNotaria" value="<?=$numNot;?>"/>
  <input type="hidden" name="tiponot" value="<?=$tipoNot;?>"/>
  <input type="hidden" name="municipio" value="<?=$idMunicipio;?>"/>
<? } ?>
  <!--  $nombrenot = $nombreNot;
$apPatNot = $apPNot;
$apMatNot = $apMNot;
$numNotaria =
$tiponot = $tipoNot;-->
</form>
</body>
</html>