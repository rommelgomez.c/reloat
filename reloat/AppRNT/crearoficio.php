<?php

//error_reporting(0);
include '../Config/testalib.php'; //Se incluye el archivo con las diferentes funciones que se utilizan 
	//session_start();
	//if (session_is_registered('activa'))
	   $activausr = 1;
	//else
	  // header("Location: ../index.php"); 
require('./scripts/fpdf153/fpdf.php');

conecta('avisos');


class PDF extends FPDF {
 function Header(){
  global $title;
  $this->SetMargins(3,25,3);
  // x,y,w,h
  $this->Image('imagenes/fondorenap.jpg',0,0,210,320);
  $this->Ln(2);
 }
  
 function textoTitulo(){ //Reporte de busqueda
   global $title;
   $this->SetFont('Arial','B',12);
   $w=$this->GetStringWidth($title);
   $this->SetDrawColor(150,150,150);
   $this->SetFillColor(150,150,150);
   $this->SetTextColor(255,255,255);
   $this->SetLineWidth(.3);
   $this->Cell(1,0,'',0,0,'C',0);
   $this->Cell(202,5,$title,0,1,'C',1);
  }

   function subtitulo($texto){
    $this->SetFont('Arial','B',9);
    $this->SetDrawColor(165,42,42);
    $this->SetFillColor(165,42,42);
    $this->SetTextColor(255,255,255);
    $this->SetLineWidth(.3);
    $this->Cell(48,4,"* $texto",1,1,'L',1);
    $this->SetTextColor(0,0,0);
  }
  
	function Footer()
	{
		//Posici�n: a 1,5 cm del final
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Arial','I',7);
		//N�mero de p�gina
		$this->Cell(0,10,'Este documento se integra de {nb} fojas P�gina '.$this->PageNo().' ',0,0,'C');
	}  
	
function BasicTable($header,$data)
{
    //Cabecera
    foreach($header as $col)
        $this->Cell(40,7,$col,1,'C');
    $this->Ln();
    //Datos
	//$this->Cell(40,6,print($data),1);
    $dx=0;
	$dy=0;
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(40,6,$col,1);
        $this->Ln();
    }	
}


function BasicTable2($header,$data)
{
    //Cabecera
    foreach($header as $col)
        $this->Cell(50,7,$col,1,'C');
    $this->Ln();
    //Datos
	//$this->Cell(40,6,print($data),1);
    $dx=0;
	$dy=0;
    foreach($data as $row)
    {
        foreach($row as $col)
            $this->Cell(50,6,$col,1,'C');
        $this->Ln();
    }	
}

	
}//Termina la clase

//Para la fecha y hora
$dia=date("d");
$mes=date("m");
$ano=date("y");
$hora=date("H");
$min=date("i");

$meses= array("enero", "febrero","marzo","abril","mayo","junio","julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");

$i=0;
foreach($meses as $m){
  $i++;
  if($i<10) $i='0'.$i;
  $meses[$i] = $m;
}

//  ################# Extracci�n de datos del aviso de poder ##################
	

	//QUERY DEL TESTADOR
	$sPdn = "SELECT * FROM modificaciones WHERE idModificacion = $idModificacion"; 
	$rPdn = mysql_query($sPdn) or die(mysql_error());
	
	

    //T�tulos de las columnas
    $titulos=array('NOMBRE','APELLIDO PATERNO','APELLIDO MATERNO','CURP','NACIONALIDAD');
	$titulos2=array('NOMBRE','APELLIDO PATERNO','APELLIDO MATERNO','CURP');
	$titulos3=array('NOTARIO','TIPO','FECHA DE REVOCACI�N','NOTA');
	
	//SACO LOS DATOS DEL PODERDANTE
	$llevo=0;
    while($datoPdn = mysql_fetch_row($rPdn))
	{
	 $datospo[$llevo][0]=decripta($datoPdn[1],"");
	 $datospo[$llevo][1]=decripta($datoPdn[2],"");
	 $datospo[$llevo][2]=decripta($datoPdn[3],"");
	 $datospo[$llevo][3]=decripta($datoPdn[5],"");
	 $datospo[$llevo][4]=decripta($datoPdn[6],"");
	$idP = $datoPdn[0];
	
	 $llevo++;
	}
	

	

	
// ############################ FIN de la Extracci�n ###################
 
	 if($tipo=='C')
	 {
	 $archivo = "/home/proyectos/renapon/htdocs/documentos/i".$datoPod[9]. ".pdf"; 
	 $archivow = "./documentos/i".$datoPod[9].".pdf"; 
	 $opcion = "INFORME DE INGRESO DE REGISTRO ";
	 $conocimiento = "QUE SE INTEGRO CON �XITO EN LA BASE DE DATOS NACIONAL EL AVISO DE PODER:";
	 }
	 else if($tipo=='R')
	 {
	 $archivo = "/home/proyectos/renapon/htdocs/documentos/r".$datoPod[9]. ".pdf"; 
	 $archivow = "./documentos/r".$datoPod[9].".pdf"; 	 
	 $opcion = "INFORME DE EXISTENCIA Y VIGENCIA DE REGISTRO ";	 
	 $conocimiento = "QUE DERIVADO DE LA CONSULTA REALIZADA SE DETERMIN� QUE EL AVISO DE PODER EXISTE Y ESTA VIGENTE CON LOS SIGUIENTES DATOS:";	 
	 }
	 
     $y = 500; 
	 $registro = 1;
        //Comenzando librer�a FPDF
		$pdf=new PDF();
		//Estableciento propiedades del documento
		$pdf->SetTitle('Registro Local de Avisos de Testamentos');
		$pdf->SetAuthor('Registro Local de Avisos de Testamentos');
		$pdf->SetCreator('Registro Local de Avisos de Testamentos');
		$pdf->SetSubject($opcion.' EN EL REGISTRO NACIONAL');
	
		//Generando documento
		$pdf->AddPage();
		$pdf->Ln(19);
		$pdf->textoTitulo();
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Arial','',10);
		$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(100,4,"M�xico, DF, $dia de $meses[$mes] de 20".$ano." a las $hora:$min  horas",0,0,'L');
		$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(200,4.5,$snombre,0,0);
		$pdf->Ln(5);
		$pdf->Cell(147,4.5,$spuesto,0,0);
		$pdf->SetFont('Arial','',10);
    	$estadoU = $snomEstado;
		$pdf->Ln(5);
		$pdf->Cell(100,4.5,$sdependencia.", ".$estadoU,0,0);		
 	    $pdf->Ln(7);

		//Comenzando texto
		$pdf->SetFont('Arial','',10);
		$pdf->MultiCell(0,5,"EL QUE SUSCRIBE EN MI CAR�CTER DE DIRECTOR GENERAL DE COMPILACI�N Y CONSULTA DEL ORDEN JUR�DICO NACIONAL DE LA SECRETAR�A DE GOBERNACI�N, RESPONSABLE DEL REGISTRO NACIONAL DE AVISOS DE PODERES NOTARIALES, HAGO DE SU CONOCIMIENTO ".$conocimiento);
	
	 $dist=4;
	 $distX=50;
	 
 	 $w = 0;
	 $z = 1;
	 $q = 0;
	 $letra=9;

	 
	 $pdf->subtitulo('DATOS DE PODERDANTES');
	 $pdf->Ln(1);
     $pdf->BasicTable($titulos,$datospo);
	 $pdf->Ln(1);	

 
	 $pdf->SetFont('Arial','B',$letra);
	 $pdf->Cell(210,4,'Dr. Eduardo Castellanos Hern�ndez',0,1,'C');
	 $pdf->SetFont('Arial','',$letra);
	 $pdf->Cell(210,4,'Director General de Compilaci�n y Consulta del Orden Jur�dico Nacional',0,1,'C');
	 $pdf->AliasNbPages();
     $pdf->OutPut($archivo);
	 header('Location: '.$archivow);
?>
	
