<? include ("../Config/testalib.php");  //Se incluye el archivo con las diferentes funciones que se utilizan 
   session_start();
   conecta ("avisos");  //Funcion para conectarse a la base de datos
   $estado = estadouser($idUsr);  //Funcion para obtener el estado al que pertenece el usuario
   
   //echo "user ".$idUsr;
?>
<html>
<head>
<title>Registro Nacional de Avisos de Testamento</title>
<style type="text/css"><!--
.Estilo4 {font-size: 14px}
--></style>
</head>
<link href="./scripts/Stylesheetpe.css" rel="stylesheet" type="text/css"></head>
<script language="JavaScript">
//Valida que se hayan llenado los campos necesarios
function validar() 
{	campo = uploading.archivo;
	var respuesta; 
	respuesta=confirm("IMPORTANTE: RECUERDA QUE LAS FECHAS DEBEN DE TENER UN FORMATO 0000-00-00 Y LOS NOMBRES DEBEN ESTAR COMPLETOS SIN ACENTOS. Estas seguro que deseas importar esta información?");
    if (respuesta == true)
    {	if (campo.value == '' )
	    {	alert("El campo del nombre del acrhivo no ha sido capturado");
            return false;
		}
		else
		{	uploading.Enviar.disabled=true
			return true;
		}
	}
	else
	{	return false;	}
}   
</script>
<body leftmargin="0" topmargin="0">
<? include ('./head.php'); //Se incluye el banner del encabezado?>
<form onSubmit="return validar();" action="importar.php" enctype="multipart/form-data" method="post" name="uploading"> 
  <table width="750" height="147" border="0" bgcolor="#E8E8E8">
    <tr> 
      <td colspan="3" bgcolor="#3983C5"><font color="#FFFFFF">CARGA DE INFORMACI&Oacute;N
        </font></span></td>
    </tr>
    <tr> 
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="3" align="center"><div align="center"><font color="#FF0000" size="2" face="Verdana, Arial, Helvetica, sans-serif">NOTA 
          IMPORTANTE: RECUERDA QUE LOS ARCHIVOS QUE SE DEBEN MIGRAR <br> SON ARCHIVOS 
          CSV GENERADOS SOBRE LA PLANTILLA QUE SE INDICA EN EL MANUAL.</font></div></td>
    </tr>
    <tr> 
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="3"><input type="hidden" name="idUsr" value="<?=$idUsr;?>"></td>
    </tr>    
    <tr> 
      <td colspan="3"><center>
          <font size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong>ARCHIVO CSV:</strong></font> 
          <input type="file" name="archivo" size="30">
        </center></td>
    </tr>
	<tr> 
      <td colspan="3"><div align="center" style="visibility:hidden;">INFORMACI&Oacute;N DEL RENAT
          <input name="estatus" type="checkbox" id="estatus" value="nacional">
      </div></td>
    </tr>   
    <tr> 
      <td colspan="3"><center><input type="submit" name="Enviar" value="IMPORTAR CSV"></center></td>
    </tr>
    <tr> 
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr> 
      <td colspan="3">&nbsp;</td>
    </tr>   
    <tr> 
      <td width="323">&nbsp;</td>
      <td width="102">&nbsp;</td>
      <td width="311" bgcolor="#E8E8E8">&nbsp;</td>
    </tr>
  </table>  <br>
  <table width="750" border="0" cellpadding="0" cellspacing="0">
     <tr bgcolor="#E8E8E8"> 
      <td colspan="3"><div align="center"><a href="./validacion/menu.php">Men&uacute;</a></div></td>
    </tr>
    <tr>
      <td><? include('./foot.php'); //Se incluye el banner del pie de página?></td>
    </tr>
  </table>
  <p>&nbsp;</p>
</form>		  
</body>
</html>