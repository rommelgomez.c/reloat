<? 
include "../Config/testalib.php";  //Se incluye el archivo con las diferentes funciones que se utilizan 
include "../Config/KXFormDBBased.class.php";  //Se incluye el archivo con las clases para la creaci�n de los campos del formulario

session_start();
if (session_is_registered('activa'))
	$activausr = 1;
else
	header("Location: ../index.php");
conecta("avisos");  //Funcion para conectarse a la base de datos
$estado=estadouser($idUsr);  //Funcion para obtener el estado al que pertenece el usuario
//echo $estado;


?>
<html>
<head>
<title>Registro Local de Avisos de Testamento</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="./scripts/Stylesheetpe.css" rel="stylesheet" type="text/css">
<style type="text/css"><!--
.Estilo1 {font-size: 12px}
--></style>
<!-- Se incluyen los archivos para las m�scaras de los campos de texto -->
<script src="scripts/mask/masks.js"></script>
<script src="scripts/mask/config.js"></script>
</head>
<script language="JavaScript">
//Pregunta al usuario si desea borrar el registro
function validar() 
{	var respuesta; 
	respuesta=confirm("Estas seguro que deseas borrar este registro?");
    if (respuesta)
    {	return true;	}
	else
	{	return false;	}	
}

//Bloquea o desbloquea algunos campos del formulario
function bloquea()
{	form = document.form1;
	
	if (form.bpor.value == 2)
	{	form.nombre.disabled = true;
		form.appaterno.disabled = true;
		form.apmaterno.disabled = true;
		form.nombre.value = "";
		form.appaterno.value = "";
		form.apmaterno.value = "";
	}
	else
	{	form.nombre.disabled = false;
		form.appaterno.disabled = false;
		form.apmaterno.disabled = false;
	}
}

//Convierte a mayusculas lo que se escriba en los campos de texto
function Mayusculas(nombre)
{	var alta= new String();
    alta=nombre.value;
	alta= alta.replace(/(^\s*)|(\s*$)/g,""); 	
    nombre.value=alta.toUpperCase();
}
</script>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<p>
<?	include('./head.php');	//Se incluye el banner del encabezado

$fdb = new KXFormDBBased("testamentos"); //Creamos el objeto fdb para la tabla de testamentos	

?>
</p>
<? //if(isset($buscar)){ $buscar = $buscar; } else { $buscar = ""; } ?>
<form name="form1" method="post" action="./admonregistros.php?idUsr=<?=$idUsr;?>&letra=<?=$letra;?>&desde=<?=$desde;?>&hasta=<?=$hasta;?>&buscando=<?=$buscando;?>">
  <table width="750" border="0" cellpadding="0" cellspacing="0" bgcolor="#E8E8E8">
    <tr bgcolor="#663300">
      <td colspan="4" bgcolor="#3983C5"><font color="#FFFFFF"><strong>ADMINISTRACI�N DE REGISTROS</strong></font></td>
    </tr>

    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
	
    <tr>
      <td colspan="4" align="center">Seleccione una o en su caso todas las Opciones para que se desplieguen los Registros:</td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td align="right" colspan="2" ><strong>Letra del Apellido Paterno: </strong></td>
      <td width="376" colspan="2">&nbsp;&nbsp;
	  <select name="letra">
	<option value="">Seleccione..</option>
	<option value='A'>A</option>
	<option value='B'>B</option>
	<option value='C'>C</option>
	<option value='D'>D</option>
	<option value='E'>E</option>
	<option value='F'>F</option>
	<option value='G'>G</option>
	<option value='H'>H</option>
	<option value='I'>I</option>
	<option value='J'>J</option>
	<option value='K'>K</option>
	<option value='L'>L</option>
	<option value='M'>M</option>
	<option value='N'>N</option>
	<option value='O'>O</option>
	<option value='P'>P</option>
	<option value='Q'>Q</option>
	<option value='R'>R</option>
	<option value='S'>S</option>
	<option value='T'>T</option>
	<option value='U'>U</option>
	<option value='V'>V</option>
	<option value='W'>W</option>
	<option value='X'>X</option>
	<option value='Y'>Y</option>
	<option value='Z'>Z</option>
	  </select>	  </td>
    </tr>
	    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td align="right" colspan="2"><strong>Fecha de Escritura:  </strong></td>
      <td colspan="2">&nbsp;Desde:&nbsp;<input name="desde" type="text"  onFocus="segFechaMask(this)" size="14" maxlength="10"  >
	  Hasta: 
      &nbsp;<input name="hasta" type="text" onFocus="segFechaMask(this)" size="14" maxlength="10"  ></td>
    </tr>
	<tr>
      <td colspan="4" align="center">&nbsp;</td>
    </tr>
	    <tr>
      <td colspan="2" align="right"><b>No. de Escritura: </b>&nbsp;</td>
      <td colspan="2" align="left">&nbsp;<input name="escri" type="text" size="5"></td>
    </tr>
	    <tr>
      <td colspan="4" align="center">&nbsp;</td>
    </tr>
	    <tr>
      <td colspan="4">&nbsp; <input type="hidden" name="buscando" value="0"></td>
    </tr>
	 <tr>
      <td colspan="4" align="center">&nbsp;<input type="submit" name="Submit" value="BUSCAR"></td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
  </table>
</form>
<p> 
<? 
if (isset($buscando))
{	//Hacemos la consulta seg�n los datos escritos
						 	
if (isset($_POST["letra"])){ if($letra != ""){	$xletra = " AND apPaterno like '$letra%'"; } else { $xletra = ""; }}
if (isset($_POST["desde"])){ if($desde != ""){ $desd = " AND fechaEscritura >= '$desde'";	} else { $desd = ""; }}
if (isset($_POST["hasta"])){ if($hasta != ""){ $hast = " AND fechaEscritura <= '$hasta'";	} else { $hast = ""; }}			
if (isset($_POST["escri"])){ if($escri != ""){	$xescri = " AND escritura = '$escri'"; } else { $xescri = ""; }}	

if (isset($_GET["letra"])){ if($letra != ""){	$xletra = " AND apPaterno like '$letra%'"; } else { $xletra = ""; }}	
if (isset($_GET["desde"])){ if($desde != ""){ $desd = " AND fechaInstrumento >= '$desde'";	} else { $desd = ""; }}
if (isset($_GET["hasta"])){ if($hasta != ""){ $hast = " AND fechaInstrumento <= '$hasta'";	} else { $hast = ""; }} 	
if (isset($_GET["escri"])){ if($escri != ""){	$xescri = " AND escritura = '$escri'"; } else { $xescri = ""; }}	
		
	$sqlquery = "SELECT * FROM testamentos WHERE idEstado != 0 $xletra $desd $hast $xescri ORDER BY apPaterno";

	//echo $sqlquery;
		
	$_pagi_sql = $sqlquery;
}
$_pagi_cuantos = 50;
$_pagi_nav_num_enlaces = 6;
$flag = 1;
//Incluimos el script de paginaci�n. �ste ya ejecuta la consulta autom�ticamente
if (isset($buscando))
	{include("paginator.inc.php");}	
else	 
{  	if (isset($seg))
   include("paginator.inc.php");
}			


if (isset($_pagi_totalReg))
{   if ($_pagi_totalReg != 0)
    {	
	
	//Imprime dentro de la misma p�gina los resultados de la b�squeda
	echo "<br><p style='width:750px; text-align:center'><font color='#FF0000' size='2' face='Verdana, Arial, Helvetica, sans-serif'>&nbsp; <b>N�mero de coincidencias: " .  $_pagi_totalReg ;
		echo "</b></font><br><br>";
	//Bot�n de generar excel
	echo "<input type='button' value='Generar Excel con los resultados' onclick='window.open(\"generaExcel.php?letra=$letra&desde=$desde&hasta=$hasta&escri=$escri\")'></p>";
		
		echo "<table border=1 width=750>";
		echo " <tr>";
		echo "  <td  bgcolor='#CCCCCC'><center><b>N�MERO DE CONTROL <br>INTERNO</b></center></td>\n";
		echo "  <td  bgcolor='#CCCCCC'><center><b>NOMBRE DEL TESTADOR</b></center></td>\n";
		echo "  <td  bgcolor='#CCCCCC'><center><b>ESCRITURA</b></center></td>\n";
		echo "  <td  bgcolor='#CCCCCC'><center><b>FECHA DE ESCRITURA</b></center></td>\n";
		echo "  <td  bgcolor='#CCCCCC'><center><b>FECHA DE INGRESO</b></center></td>\n";
		echo "  </tr>\n";
		while ($dato=mysql_fetch_array($_pagi_result)) 
		{	
			echo "  <tr>\n";
			echo "    <td><center>".$dato["procNumIntEdo"]."</center></td>\n";
			echo "    <td>".$dato["apPaterno"]." ".$dato["apMaterno"]. " " .$dato["Nombre"]."</td>\n";
			echo "    <td><center>".$dato["escritura"]."</center></td>\n";
		 	echo "    <td><center>".$dato["fechaInstrumento"]."</center></td>\n";
		 	echo "    <td><center>".$dato["fechaIngreso"]."</center></td>\n";
			echo " </tr>";
		}
	echo "</table>\n";

	}
	}
	if(isset($_pagi_navegacion))
	{ ?>
	
	<p style="width:750px; text-align:center">

	<font color="#000000"><?=$_pagi_navegacion?></font></p>
		<? }
	mysql_close ();	?>
</p><br>
<table width="750" height="15" border="0" cellpadding="0" cellspacing="0">
  <tr bgcolor="#E8E8E8">
    <td><center>&nbsp; <a href="./validacion/menu.php">Men&uacute;</a></center></td>   

</tr>  
</table>
<? include ('./foot.php'); //Se incluye el banner para el pie de p�gina?>
<p align="left">&nbsp;</p>
</body>
</html>