<?	include '../Config/testalib.php'; //Se incluye el archivo con las diferentes funciones que se utilizan 
	session_start();
	if (session_is_registered('activa'))
	   $activausr = 1;
	else
	   header("Location: ../index.php"); ?>
<html>
<head>
<title>Registro Nacional de Testamentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css"><!--
td {font-size: xx-small; font-family: Verdana; font-weight: normal;}
--></style><link href="./scripts/Stylesheetpe.css" rel="stylesheet" type="text/css">
</head>
<body>
<?  $separado = " ";
    conecta ("avisos");  //Funcion para conectarse a la base de datos
	//Busca los datos del aviso que se va a revisar
    $SQL = "SELECT * FROM $tabla where idTestamento = ".$numero.""; 
	$rs = mysql_query($SQL);
	$nRows = mysql_num_rows($rs);
	if($nRows == 0) die ("<br>NO SE ENCONTRARON REGISTROS QUE COINCIDAN CON LOS DATOS ENVIADOS<br>\n");
	$dato=mysql_fetch_array($rs); //Muestra todos los datos del aviso ?>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="16" colspan="4"> <font size="1"><strong>DATOS DEL TESTADOR</strong></font></td>
  </tr>
  <tr> 
    <td>Nombre: </td>
    <td colspan="3"><? echo "|N: ";
	echo $dato['Nombre'];
	echo " |AP: ";
	echo $dato['apPaterno'];
	echo " |AM: ";			
  	echo $dato['apMaterno'];
	echo " |AC: ";
	echo $dato['apConyuge'];?></td>
  </tr>
  <tr> 
    <td width="18%">Lugar de nacimiento:</td>
    <td width="40%"><?=$dato['lugarNacimiento'];?></td>
    <td width="11%">Nacionalidad:</td>
    <td width="31%"><?=$dato['nacionalidad'];?></td>
  </tr>
  <tr> 
    <td>Fecha de nacimiento:</td>
    <td><?=$dato['fechaNacimiento'];?></td>
    <td>Estado civil:</td>
    <td><?=$dato['edoCivil'];?></td>
  </tr>
  <tr> 
    <td>Curp:</td>
    <td colspan="3"><?
	if(empty($dato['curp']))
	{	echo "----------";	}
	else
	{	echo $dato['curp'];	}
	?></td>
  </tr>
  <tr> 
    <td>Nombre del Padre: </td>
    <td colspan="3"><?
	echo "|N: ";
	echo $dato['nomPadre'];
	echo " |AP: ";
    echo $dato['apPatPadre'];
	echo " |AM: ";
    echo $dato['apMatPadre'];?></td>
  </tr>
  <tr> 
    <td height="20">Nombre de la Madre:</td>
    <td colspan="3"><?
	echo "|N: ";
	echo $dato['nomMadre'];
	echo " |AP: ";		
	echo $dato['apPatMadre'];
	echo " |AP: ";
	echo $dato['apMatMadre'];
	echo " |AC: ";
	echo $dato['apConMadre'];?></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td colspan="4"> <font size="1"><strong>DATOS DEL INSTRUMENTO</strong></font></td>
  </tr>
  <tr> 
    <td width="18%">Tipo de Testamento:</td>
    <td width="40%"><?=$dato['tipoTestamento'];?></td>
    <td width="18%">No. de escritura:</td>
    <td width="24%"><?=$dato['escritura'];?></td>
  </tr>
  <tr> 
    <td>Volumen o tomo:</td>
    <td><?=$dato['oTomo'];?></td>
    <td>Fecha y hora de escritura:</td>
    <td><?=$dato['fechaInstrumento']; echo '&nbsp; &nbsp '.$horaInstrumento;?></td>
  </tr>
  <tr> 
    <td height="24">Lugar de otorgamiento:</td>
    <td><?=$dato['lugarOtorgamiento'];?></td>
    <td>Disposiciones de contenido irrevocable: </td>
    <td><?=$dato['contenidoIrrevocable'];?></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td colspan="4"> <font size="1"><strong>DATOS DEL NOTARIO</strong></font></td>
  </tr>
  <tr> 
    <td width="18%">No. de Notario:</td>
    <td width="40%"><? 
	  $sqlnot = "SELECT * FROM catnotarios WHERE idNotario=".$dato["idNotario"];
	  $rsnot=mysql_query($sqlnot) or die(mysql_error ());
	  $dato1=mysql_fetch_array($rsnot);
	  califica ($dato1['numNotaria'] ,"");?></td>
    <td width="13%">Tipo Notario:</td>
    <td width="29%"><?=$dato1['tipoNotario'];?></td>
  </tr>
  <tr> 
    <td>Entidad Federativa:</td>
    <td><?
    if (empty ($dato1['idEstado']))
	{	echo "----------------";	}
	else
	{	$sqlquery2="SELECT Estado FROM catestado WHERE idEstado =".$dato1["idEstado"];
		$queryresultado2 = mysql_query($sqlquery2);
		$dato2=mysql_fetch_array($queryresultado2);
        echo $dato2['Estado'];
	}?></td>
    <td>Municipio:</td>
    <td><?
 	if (empty ($dato1['idMunicipio']))
		{echo "----------------";}
	else	 
	{	$sqlquery2="SELECT Municipio FROM catmunicipios WHERE idMunicipio =". $dato1['idMunicipio'];
		$queryresultado2 = mysql_query($sqlquery2);
		$dato2=mysql_fetch_array($queryresultado2);
		echo $dato2['Municipio'];
	}?></td>
  </tr>
  <tr> 
    <td height="18" >Nombre del Notario:</td>
    <td colspan="3"><?
    echo "|N: ";
	echo $dato1['nombre'];
	echo " |AP: ";
	echo $dato1['apPaterno'];
	echo " |AM: ";			
  	echo $dato1['apMaterno'];?></td>
  </tr>
  <tr>
    <td height="20" >Juez:</td>
    <td colspan="3"><?=$dato['juez'];?></td>
  </tr>
</table>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td colspan="4"> <font size="1"><strong>DATOS DE REGISTRO DE LA ENTIDAD FEDERATIVA 
      DE PROCEDENCIA</strong></font></td>
  </tr>
  <tr> 
    <td width="18%">No. de Control Interno:</td>
    <td width="40%"><?=$dato['procNumIntEdo'];?></td>
    <td width="15%">Fecha y hora de registro:</td>
    <td width="27%"><?=$dato['procFechaReg']; echo '&nbsp; &nbsp; '.$horaRegistro;?></td>
  </tr>
  <tr> 
    <td>Entidad Federativa:</td>
    <td><?
  	$sqlquery2="SELECT Estado FROM catestado WHERE idEstado = ".$dato["idEstado"];
	$queryresultado2 = mysql_query($sqlquery2);
	$dato2=mysql_fetch_array($queryresultado2);
    if (empty ($dato['idEstado']))
		{echo "-------------------";}
	else	 
		{echo $dato2['Estado'];}?></td>
    <td>Municipio:</td>
    <td><? $sqlquery2="SELECT Municipio FROM catmunicipios WHERE idMunicipio = ".$dato['idMunicipio'];
	$queryresultado2 = mysql_query($sqlquery2);
	$dato2=mysql_fetch_array($queryresultado2);
    if (empty ($dato['idMunicipio']))
		{echo "-------------------";}
	else	 
		{echo $dato2['Municipio'];}?></td>
  </tr>
  <tr> 
    <td height="20" >Oficina de registro:</td>
    <td colspan="3"><?=$dato['ofiRegistro'];?></td>
  </tr>
  <tr> 
    <td height="18">No. de control interno:</td>
    <td><strong>Por asignarse al ser validado </strong></td>
    <td> Capturista:</td>
    <td><?=$dato['capturista'];?></td>
  </tr>
  <tr> 
    <td width="18%" height="18">Fecha de ingreso:</td>
    <td width="30%"><?=$dato['fechaIngreso'];?></td>
    <td width="16%">Fecha de registro:</td>
    <td width="36%"><?=$dato['fechaRegistro'];?></td>
  </tr>  
</table>
<? mysql_close(); ?>
</body>
</html>