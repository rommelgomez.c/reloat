<?
/**
 * Genera campos de formulario ya sea basado en MYSQL o por metodos est�ticos
 * @author Javier Flores I. <desarrollo@webservice.com.mx>
 * @version 0.9
 * @package Clases Comunes MYSQL
*/



class KXFormDBBased{

/**
* Tabla principal a analizar
* @access private
* @var string
*/
var $tb;

/**
* Cadena de inicio en mensaje de error
* @access private
* @var string
*/
var $errorMsg = "ERROR EN: ";

/**
* Numero m�ximo de caracteres para un varchar
* @access private
* @var integer
*/
var $varcharMaxSize = 255;

/**
* Etiqueta Inicial para los SELECT, su valor Inicial es vac�o
* @access private
* @var string
*/
var $selectIniLabel = "";

/**
* Etiqueta Inicial para los SELECT, su valor Inicial es vac�o
* @access private
* @var boolean
*/
var $debug = false;







/**
* Constructor
* @param string $tb Tabla principal
*/
function KXFormDBBased($tb){

	if ($tb != ""){
		$this->tb = $tb;
	}

}


/**
* Inicia Debugging
* @access public
* @return void
*/
function startDebug(){
	
	echo '
		<script>
			alert("DEBUG SOLO FUNCIONA PARA CAMPOS RELACIONADOS A BD :-)");
		</script>
	';
	
	$this->debug = true;
}



/**
* Detiene Debugging
* @access public
* @return void
*/
function stopDebug(){
	$this->debug = false;
}



/**
* Genera un campo TEXT o TEXTAREA en base a un campo de la tabla definida
* @access public
* @param integer $fieldNumber N�mero del campo 
* @param string $class Estilo css
* @param string $value Valor del Campo
* @param string $extraParams Par�metros extra por ejemplo JS
* @param string $forcedFieldName Forzar el nombre del campo a �ste
* @return string 
*/
function genStringField($fieldNumber,$class="",$value="",$extraParams="",$forcedFieldName=""){
	
	$ret = "";
	
	$this->_exeQuery("SELECT * FROM ".$this->tb);
	$fieldType = $this->_getFieldType($fieldNumber);
	$fieldName = $this->_getFieldName($fieldNumber);
	$fieldLength = $this->_getFieldLength($fieldNumber);
	
	if ($forcedFieldName != ""){
		$fieldName = $forcedFieldName;
	}
		
	if ($fieldLength<=$this->varcharMaxSize){
		$ret = $this->addTHPInput($fieldName,$value,"text",$class,$fieldLength,$extraParams);
	}
	else{
		$ret = $this->addTextArea($fieldName,$class,$value,$extraParams);
	}
	
	if ($this->debug){
		$ret .= $this->_setDebug($fieldName,$fieldLength,$class,"",$extraParams);
	}
	
	return $ret;
	
}



/**
* Genera un campo TEXT o TEXTAREA en base a un campo de una tabla for�nea
* @access public
* @param string $tb Tabla
* @param string $field Campo
* @param string $value Valor
* @param string $class Estilo css
* @param string $extraParams Par�metros extra por ejemplo JS
* @param string $forcedName Forzar el nombre del campo a �ste
* @return string 
*/
function genOutStrField($tb,$field,$value="",$class="",$extraParams="",$forcedName=""){
	
	$ret = "";
	
	$query = "SELECT $field FROM $tb";
	
	$this->_exeQuery($query);
	$fieldLength = $this->_getFieldLength(0);
	
	if ($forcedName!=""){
		$fieldName = $forcedName;
	}
	else{	
		$fieldName = $this->_getFieldName($fieldNumber);
	}
		
	if ($fieldLength<=$this->varcharMaxSize){
		$ret = $this->addTHPInput($fieldName,$value,"text",$class,$fieldLength,$extraParams);
	}
	else{
		$ret = $this->addTextArea($fieldName,$class,$value,$extraParams);
	}
	
	if ($this->debug){
		$ret .= $this->_setDebug($fieldName,$fieldLength,$class,"",$extraParams);
	}
	
	return $ret;
		
}



/**
* Genera un campo SELECT,Grupo de CHECKBOXes o Grupo de RADIOs en base a una tabla for�nea
* @access public
* @param string $tb Tabla
* @param string $fValue Campo para el valor
* @param string $fLabel Campo para la etiqueta
* @param string $class Estilo css
* @param string $type Tipo del (los) campo(s) 'select' | 'checkbox' | 'radio', por default 'select'
* @param string $extQuery Complemento del query principal para hacer un filtro ej. WHERE campox='valorx'
* @param array $selected Arreglo que contiene los valores para definir los campos que estar�n seleccionados ej. $selected=array(1,2,'str',...)
* @param string $extraParams Par�metros extra por ejemplo JS
* @param integer $size En caso de $type='select' es la altura del campo select
* @param boolean $multiple En caso de $type='select' define si es de seleccion multiple
* @param string $forcedName Forzar el nombre del campo a �ste
* @return string 
*/
function genForeignField($tb,$fValue,$fLabel,$class="",$type="select",$extQuery="",$selected=array(),$extraParams="",$size=1,$multiple=false,$forcedName=""){
	$ret = "";
	
	$query = "SELECT $fValue,$fLabel FROM $tb $extQuery";
	$this->_exeQuery($query);
	$ar = array();
	
		if (mysql_num_rows($this->result)){
			for ($i=0;$i<=mysql_num_rows($this->result)-1;$i++){
				$ar[$i]["value"] = $this->_fetchField($i,$fValue);
				$ar[$i]["label"] = $this->_fetchField($i,$fLabel);
			}
		}
		
		if ($forcedName!=""){
			$idField = $forcedName;
		}
		else{
			$idField = $fValue;
		}
	
	switch ($type){
	case "select":
		$ret = $this->addSelectList($idField, $ar, $class, $selected, $extraParams, $size,$multiple);
	break;
	case "checkbox":
		$ret = $this->addCheckBox($idField, $ar,"",$class,$selected,$extraParams);
	break;
	case "radio":
		$ret = $this->addRadio($idField, $ar,"", $class,$selected[0], $extraParams);
	break;
	}
	
	if ($this->debug){
		$ret .= $this->_setDebug($idField,0,$class,"",$extraParams);
	}		
	
	return $ret;	  
	
}




/**
* Define la etiqueta de primer OPTION en un SELECT y se mantiene hasta que se vuelva a llamar
* @access public
* @param string $str Etiqueta
* @return void 
*/
function setSelectIniLabel($str){//
  	$this->selectIniLabel = $str;
}




/**
* Genera un campo TEXT | HIDDEN | PASSWORD 
* @access public
* @static
* @param string $idField Nombre del campo
* @param string $value Valor del campo
* @param string $type Tipo del campo 'text' | 'hidden' | 'password', por default 'text'
* @param string $class Estilo css
* @param integer $maxlength En caso de 'text' | 'password' N�mero de caracteres m�ximo, por default 100
* @param string $extraParams Par�metros extra por ejemplo JS
* @param string $size Tama�o del textfield
* @return string 
*/
 function addTHPInput($idField, $value='', $type="text", $class="", $maxlength=100,$extraParams="",$size=""){
  

	switch ($type){
		
	case "text":
		return '<input id="'.$idField.'" name="'.$idField.'"   maxlength="'.$maxlength.'" type="text" value="'.$value.'" '.$extraParams.' class="'.$class.'" size="'.$size.'">';
	break;
	
	case "password":
        return '<input id="'.$idField.'" name="'.$idField.'"  maxlength="'.$maxlength.'" type="password" value="'.$value.'" '.$extraParams.' class="'.$class.'">';
	break;
	
	case "hidden":
        return '<input id="'.$idField.'" name="'.$idField.'"   type="hidden" value="'.$value.'" >';
	break;	
		
	}
	    
 }
  
  
 
/**
* Genera un campo SELECT
* @access public
* @static
* @param string $idField Nombre del campo
* @param array $ar Valores y etiquetas con formato $ar[0]['label'] = 'valor1'; $ar[0]['value'] = '1; ...
* @param string $class Estilo css
* @param array $selected Arreglo que contiene los valores para definir los campos que estar�n seleccionados ej. $selected=array(1,2,'str',...)
* @param string $extraParams Par�metros extra por ejemplo JS
* @param integer $size La altura del campo select, por default 1
* @param boolean $multiple Define si es de seleccion multiple, por default false
* @return string 
*/
 function addSelectList($idField, $ar, $class="", $selected=array(), $extraParams="", $size=1,$multiple=false){
  
    if($ar=='' || count($ar)==0){
      return false;
    }
    else{
	
	  if ($size>0){
	  	$SelSize = 'size="'.$size.'"';
	  }	
	  else{
	  	$SelSize = "";
	  }
	  
	  if ($multiple){
	  	$isMultiple = "multiple";
	  }
	  else{
	  	$isMultiple = "";
	  }
	
      $strRet = '<select id="'.$idField.'" name="'.$idField.'" '.$SelSize.' '.$isMultiple.' '.$extraParams.' class="'.$class.'">';
	  
	  if ($size<=1){
		 if (isset($this)){
			if(isset($this->selectIniLabel)){
				$strRet.= '<option value="">'.$this->selectIniLabel.'</option>';
			}
		}
	  }
	  
      for($i=0; $i<count($ar); $i++){
	 	if (count($selected)>0){
			if (in_array($ar[$i]["value"],$selected)){
				$strRet.= '<option value="'.$ar[$i]["value"].'" selected>'.$ar[$i]["label"].'</option>';
			}
			else{
				$strRet.= '<option value="'.$ar[$i]["value"].'">'.$ar[$i]["label"].'</option>';
			}
		}
	  	else{
        	$strRet.= '<option value="'.$ar[$i]["value"].'">'.$ar[$i]["label"].'</option>';
		}
      }
      $strRet.= '</select>';
	  
      return $strRet;
    }
 }//
  
  
  
  
/**
* Genera un grupo de CHECKBOXes
* @access public
* @static
* @param string $idField Nombre del campo
* @param array $ar Valores y etiquetas con formato $ar[0]['label'] = 'valor1'; $ar[0]['value'] = '1; ...
* @param string $classObj Estilo css para el objeto
* @param string $classLabel Estilo css para la etiqueta
* @param array $checked Arreglo que contiene los valores para definir los campos que estar�n seleccionados ej. $checked=array(1,2,'str',...)
* @param string $extraParams Par�metros extra por ejemplo JS
* @return string 
*/ 
 function addCheckBox($idField, $ar, $classObj="",$classLabel="", $checked=array(), $extraParams=""){
  
    if($ar=='' || count($ar)==0){
      return false;
    }
    else{

	if ($classObj!= ""){
		$theObjClass = $classObj;
	}
	else{
		$theObjClass = "";
	}
      for($i=0; $i<count($ar); $i++){
			if (in_array($ar[$i]["value"],$checked)){
				$strRet .= '<label class="'.$classLabel.'" id='.$idField.'[]><input type=checkbox id='.$idField.'[] name='.$idField.'[] value="'.$ar[$i]["value"].'" '.$extraParams.' '.$isProcess.' checked class="'.$theObjClass.'">'.$ar[$i]["label"].'</label><br>';
			}
			else{
				$strRet .= '<label class="'.$classLabel.'"><input type=checkbox id='.$idField.'[] name='.$idField.'[] value="'.$ar[$i]["value"].'" '.$extraParams.' '.$isProcess.' class="'.$theObjClass.'">'.$ar[$i]["label"].'</label><br>';
			}
	  
      }
	  
	  
      return $strRet;
    }
 }
  
  
  
  
/**
* Genera un grupo de RADIOes
* @access public
* @static
* @param string $idField Nombre del campo
* @param array $ar Valores y etiquetas con formato $ar[0]['label'] = 'valor1'; $ar[0]['value'] = '1; ...
* @param string $classObj Estilo css para el objeto
* @param string $classLabel Estilo css para la etiqueta
* @param array $checked Arreglo que contiene los valores para definir los campos que estar�n seleccionados ej. $checked=array(1,2,'str',...)
* @param string $extraParams Par�metros extra por ejemplo JS
* @return string 
*/  
 function addRadio($idField, $ar, $classObj="", $classLabel="", $checked="", $extraParams=""){
  
    if($ar=='' || count($ar)==0){
      return false;
    }
    else{	
      for($i=0; $i<count($ar); $i++){
	  	if ($ar[$i]["value"] == $checked){
			$strRet .= '<label class="'.$classLabel.'"><input type=radio id="'.$idField.'" name="'.$idField.'" value="'.$ar[$i]["value"].'" '.$extraParams.' '.$isProcess.' checked class="'.$classObj.'"> '.$ar[$i]["label"].'</label>';
		}
	  	else{
			$strRet .= '<label class="'.$classLabel.'"><input type=radio id='.$idField.' name="'.$idField.'" value="'.$ar[$i]["value"].'" '.$extraParams.' '.$isProcess.' class="'.$classObj.'"> '.$ar[$i]["label"].'</label>';
		}
      }
      return $strRet;
    }
 }
  
  
  
 
 /**
* Genera un TEXTAREA
* @access public
* @static
* @param string $idField Nombre del campo
* @param string $class Estilo css
* @param string $value Valor del campo
* @param string $extraParams Par�metros extra por ejemplo JS
* @return string 
*/ 
 function addTextArea($idField, $class, $value="", $extraParams=""){
  
    if($idField==''){
      return false;
    }
    else{
      return '<textarea id="'.$idField.'" name="'.$idField.'" class="'.$class.'" '.$extraParams.'>'.$value.'</textarea>';
    } 
 }




/**
* Ejecuta una consulta en mysql
* @access private
* @param string $query Consulta
* @return void 
*/ 
function _exeQuery($query) {

	$this->query = $query;
	$this->result = mysql_query($query) or die ($this->errorMsg.'mysql() <br>'.$query." >> ".mysql_error());

}



/**
* Obtiene el n�mero de campos de una consulta
* @access private
* @return integer
*/ 
function _getNumFields() {

	$fields = mysql_num_fields($this->result) or die ($this->errorMsg.'getNumFields()');
	return $fields;

}



/**
* Obtiene el nombre de un campo
* @access private
* @param integer $fieldNumber 
* @return string
*/ 
function _getFieldName($fieldNumber){

	$f_name= mysql_field_name($this->result,$fieldNumber) or die ($this->errorMsg.'getFieldName()');
	return $f_name;

}
 
 
  
/**
* Obtiene la longitud de un campo en base a la consulta
* @access private
* @param integer $fieldNumber N�mero del campo en la consulta
* @return integer
*/ 
function _getFieldLength($fieldNumber){

	$f_len= mysql_field_len($this->result,$fieldNumber) or die ($this->errorMsg.'getFieldLength');
	return $f_len;

}



/**
* Obtiene el tipo de un campo en base a la consulta
* @access private
* @param integer $fieldNumber N�mero del campo en la consulta
* @return string 
*/ 
function _getFieldType($fieldNumber){

	$f_type= mysql_field_type($this->result,$fieldNumber) or die ($this->errorMsg.'getFieldType()');
	return $f_type;

}



/**
* Devuleve resultado de una fila en un arreglo
* @access private
* @param integer $id N�mero del campo
* @return array
*/ 
function _fetchRow($id) {

$data=mysql_data_seek($this->result,$id) or die ($this->errorMsg.'fetchRow()'.$this->query);
$row = mysql_fetch_array($this->result) or die ($this->errorMsg.'fetchRow()'.$this->query);

return $row;

}


/**
* Obtiene el valor de un campo de una sola fila
* @access private
* @param integer $numRow N�mero de fila
* @param string $fieldName Nombre del campo
* @return string|integer
*/ 
function _fetchField($numRow,$fieldName){

	$row = $this->_fetchRow($numRow);
	return $row[$fieldName];

}




/**
* Obtiene el numero de filas de una consulta
* @access private
* @return integer
*/ 
function _getNumRows(){

	$rows = mysql_num_rows($this->result) or die ($this->errorMsg.'getNumRows()');
	return $rows;
	
} 




/**
* Imprime el debug en pantalla
* @access private
* @param string $fieldName Nombre del elemento
* @param integer $fieldLength Longitud de caracteres del elemento
* @param string $class1 Estilo 1
* @param string $class2 Estilo 2
* @param string $extraParams Par�metros extra
* @return string
*/ 
function _setDebug($fieldName,$fieldLength,$class1,$class2,$extraParams){
	
	$ret = "<small>";
	$ret .= "<br>Nombre: ".$fieldName."<br>Longitud: ".$fieldLength."<br>Clase1: ".$class1."<br>Clase2: ".$class2."<br>Extra: ".$extraParams;
	$ret .= "</small>";
	
	
	return $ret;

}


  
}

?>