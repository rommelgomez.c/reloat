<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Instalador RELOAT</title>
<style type="text/css">
<!--
body,td,th {
	color: #000099;
}
body {
	background-color: #99CCFF;
}
-->
</style></head>

<body>
<br /><br /><br />
<table width="80%" border="0" align="center" bgcolor="#C4E1FF">
  <tr><td height="30" style="background-color:#000099; text-align:center; color:#FFFFFF">INSTALADOR DEL RELOAT</td></tr>
  
  <tr>
    <td>&nbsp;
	<bR /><br />

<? if(!isset($tipo))
{
?>

	<p align="justify"> Para poder instalar y llenar la base de datos disponible para el Sistema del Registro Local de Avisos de Testamento es necesario crear una base de datos llamada <b><i>avisos</i></b> en donde atutom�ticamente se van a crear todas las tablas, de no tener esta tabla, va a arrojar un error.</p>
	<table border="0" width="38%" align="center">
	<form action="instalador.php" method="post">
	<input type="hidden" name="tipo" value="instalar"/>
	<tr><td width="40%">Servidor:</td>
	<td width="60%"> <input type="text" name="servidor"  value="127.0.0.1:3306" /></td></tr>
	<tr><td>Usuario: </td><td><input type="text" name="usuario"  value="root" /></td></tr>
	<tr><td>Contrase�a: </td><td><input type="text" name="pass"  value="" /></td></tr>
	<tr><td>Base de Datos: </td><td><input type="text" name="base"  value="avisos" /></td></tr>
	<tr><td height="49" colspan="2"><center><input type="submit" name="Instalar" value="Instalar la base de datos" /></center></td></tr>
	</form>
	</table>

	
<? } else { 	
	
	$servidor = $_POST['servidor'];
	$usuario = $_POST['usuario'];
	$pass = $_POST['pass'];
	$base = $_POST['base'];
	
	$con = mysql_connect($servidor,$usuario,$pass);
	
	if($con)
	{
		$sq1 =  "CREATE DATABASE $base";
		$res = mysql_query($sq1) or die(mysql_error("Error al crear la base de datos $base"));
		
		
	//Me conecto a la base de datos
	$resp = mysql_select_db($base) or die(mysql_error()." Error: No se pudo accesar la base de datos");
		
	
/////////////////////////////// Creaci�n de la tabla-- ALIAS -- ///////////////////////////////////////

$alias= "CREATE TABLE IF NOT EXISTS `alias` (
  `idAlias` int(11) NOT NULL AUTO_INCREMENT,
  `idTestamento` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(100) NOT NULL,
  `apPaterno` varchar(50) NOT NULL,
  `apMaterno` varchar(50) NOT NULL,
  `apConyuge` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idAlias`,`idTestamento`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";


/////////////////////////////// Creaci�n de la tabla-- BITACORA -- ///////////////////////////////////////

$bitacora = "CREATE TABLE IF NOT EXISTS `bitacora` (
  `idLog` int(11) NOT NULL AUTO_INCREMENT,
  `idUsr` int(11) DEFAULT NULL,
  `Accion` mediumtext NOT NULL,
  `tipoAccion` varchar(50) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  PRIMARY KEY (`idLog`),
  KEY `accion` (`tipoAccion`),
  KEY `fecha` (`fecha`),
  KEY `identifica` (`idUsr`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";


/////////////////////////////// Creaci�n de la tabla-- Catestado -- ///////////////////////////////////////

$catestado = "CREATE TABLE IF NOT EXISTS `catestado` (
  `idEstado` int(11) NOT NULL AUTO_INCREMENT,
  `Estado` char(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`idEstado`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";


/////////////////////////////// Creaci�n de la tabla-- catmunicipios -- ///////////////////////////////////////

$catmunicipios = "CREATE TABLE IF NOT EXISTS `catmunicipios` (
  `idMunicipio` int(11) NOT NULL AUTO_INCREMENT,
  `Municipio` varchar(255) NOT NULL DEFAULT '',
  `idEstado` int(11) NOT NULL DEFAULT '0',
  `Tipo` enum('Municipio','Cabecera Municipal','Distrito Judicial','Partido Judicial','C?nsul') DEFAULT 'Municipio',
  PRIMARY KEY (`idMunicipio`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";


/////////////////////////////// Creaci�n de la tabla-- Catnotarios -- ///////////////////////////////////////

$catnotarios = "CREATE TABLE IF NOT EXISTS `catnotarios` (
  `idNotario` int(11) NOT NULL AUTO_INCREMENT,
  `idEstado` int(11) DEFAULT NULL,
  `idMunicipio` int(11) DEFAULT NULL,
  `numNotaria` varchar(11) DEFAULT NULL,
  `tipoNotario` varchar(50) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apPaterno` varchar(50) DEFAULT NULL,
  `apMaterno` varchar(50) DEFAULT NULL,
  `identificador` enum('N','J') DEFAULT 'N',
  PRIMARY KEY (`idNotario`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";


/////////////////////////////// Creaci�n de la tabla-- catpermisos -- ///////////////////////////////////////

$catpermisos = " CREATE TABLE IF NOT EXISTS `catpermisos` (
  `idPermiso` int(11) NOT NULL AUTO_INCREMENT,
  `Permiso` char(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`idPermiso`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";


/////////////////////////////// Creaci�n de la tabla-- cattiponot -- ///////////////////////////////////////

$cattiponot = "CREATE TABLE IF NOT EXISTS `cattiponot` (
  `idTipoNot` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idTipoNot`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";


/////////////////////////////// Creaci�n de la tabla-- consultas -- ///////////////////////////////////////

$consultas = "CREATE TABLE IF NOT EXISTS `consultas` (
  `idConsulta` int(10) NOT NULL AUTO_INCREMENT,
  `consulta` mediumtext,
  `consultaAlias` mediumtext,
  `despliege` mediumtext,
  `idUsr` int(11) NOT NULL DEFAULT '0',
  `Nombre` varchar(100) DEFAULT NULL,
  `apPaterno` varchar(50) DEFAULT NULL,
  `apMaterno` varchar(50) DEFAULT NULL,
  `apConyuge` varchar(50) DEFAULT NULL,
  `alias` text,
  `curp` varchar(30) DEFAULT NULL,
  `nacionalidad` varchar(50) DEFAULT NULL,
  `lugarNacimiento` varchar(50) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `edoCivil` enum('SOLTERO','CASADO','DIVORCIADO','UNION LIBRE','VIUDO','SOC. DE CONVIVENCIA U HOMOLOGA') DEFAULT NULL,
  `nomPadre` varchar(100) DEFAULT NULL,
  `apPatPadre` varchar(50) DEFAULT NULL,
  `apMatPadre` varchar(50) DEFAULT NULL,
  `nomMadre` varchar(100) DEFAULT NULL,
  `apPatMadre` varchar(50) DEFAULT NULL,
  `apMatMadre` varchar(50) DEFAULT NULL,
  `apConMadre` varchar(50) DEFAULT NULL,
  `ciudadano` varchar(50) NOT NULL,
  `expediente` int(20) DEFAULT NULL,
  `facreditar` enum('juez','notario','otroad') NOT NULL DEFAULT 'juez',
  `fechafa` date NOT NULL DEFAULT '0000-00-00',
  `fechabusqueda` date DEFAULT '0000-00-00',
  `horabusqueda` time DEFAULT '00:00:00',
  `archivo` varchar(100) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `estatus` enum('EN PROCESO','ENVIADO','REVISADO','LISTO','ERROR') NOT NULL DEFAULT 'EN PROCESO',
  `numacreditar` int(11) DEFAULT NULL,
  `tipoJuez` varchar(20) DEFAULT NULL,
  `municipio` varchar(50) DEFAULT NULL,
  `fechasolicitud` date DEFAULT '0000-00-00',
  `motivacion` text,
  PRIMARY KEY (`idConsulta`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";


/////////////////////////////// Creaci�n de la tabla-- modificaciones -- ///////////////////////////////////////

$modificaciones = " CREATE TABLE IF NOT EXISTS `modificaciones` (
  `idModificacion` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idTestamento` int(11) NOT NULL,
  `idEstado` int(11) NOT NULL DEFAULT '0',
  `idMunicipio` int(11) DEFAULT NULL,
  `idNotario` int(11) DEFAULT NULL,
  `Nombre` varchar(100) NOT NULL,
  `apPaterno` varchar(50) NOT NULL,
  `apMaterno` varchar(50) NOT NULL,
  `apConyuge` varchar(50) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `lugarNacimiento` varchar(200) NOT NULL,
  `edoCivil` enum('SOLTERO','CASADO','DIVORCIADO','UNION LIBRE','VIUDO','SOC. DE CONVIVENCIA U HOMOLOGA') DEFAULT NULL,
  `nacionalidad` varchar(50) NOT NULL,
  `domicilio` text,
  `sexo` enum('Masculino','Femenino') DEFAULT NULL,
  `nomPadre` varchar(100) DEFAULT NULL,
  `apPatPadre` varchar(50) DEFAULT NULL,
  `apMatPadre` varchar(50) DEFAULT NULL,
  `nomMadre` varchar(100) DEFAULT NULL,
  `apPatMadre` varchar(50) DEFAULT NULL,
  `apMatMadre` varchar(50) DEFAULT NULL,
  `apConMadre` varchar(50) DEFAULT NULL,
  `tipoTestamento` varchar(50) NOT NULL,
  `escritura` mediumint(9) DEFAULT NULL,
  `fechaInstrumento` date DEFAULT NULL,
  `horaInstrumento` time DEFAULT '00:00:00',
  `juez` varchar(50) NOT NULL,
  `lugarOtorgamiento` varchar(100) NOT NULL,
  `oTomo` varchar(100) DEFAULT NULL,
  `ofiRegistro` varchar(100) NOT NULL,
  `capturista` varchar(50) NOT NULL,
  `revisor` varchar(50) NOT NULL,
  `valido` varchar(50) NOT NULL,
  `procFechaReg` date DEFAULT NULL,
  `horaRegistro` time DEFAULT NULL,
  `procNumInt` varchar(100) DEFAULT NULL,
  `procNumIntEdo` varchar(100) NOT NULL,
  `fechaIngreso` date NOT NULL DEFAULT '0000-00-00',
  `fechaRegistro` date NOT NULL DEFAULT '0000-00-00',
  `estatus` enum('local','nacional') DEFAULT 'local',
  `notas` mediumtext,
  `curp` varchar(30) DEFAULT NULL,
  `contenidoIrrevocable` enum('Si','No') DEFAULT NULL,
  `tipoModificacion` enum('Modificacion','Eliminacion') NOT NULL,
  `fechaModificacion` date DEFAULT NULL,
  `justificacion` text,
  `estatusWeb` enum('Hecho','Pendiente','Rechazado','Multiples casos','No hayado','Enviado') DEFAULT NULL,
  `fechaEnvio` date DEFAULT '0000-00-00',
  `idTestaRENAT` int(11) NOT NULL,
  PRIMARY KEY (`idModificacion`),
  KEY `municipioTestamento_FK` (`idEstado`,`idMunicipio`),
  KEY `notarioTestamento_FK` (`idNotario`),
  KEY `escritura_FK` (`escritura`),
  KEY `bnombre` (`Nombre`),
  KEY `bapp` (`apPatPadre`),
  KEY `bapm` (`apPatMadre`),
  KEY `bnp` (`nomPadre`),
  KEY `bnm` (`nomMadre`),
  KEY `bap` (`apPaterno`),
  KEY `bamp` (`apMatPadre`),
  KEY `bamm` (`apMatMadre`),
  KEY `bnac` (`nacionalidad`),
  KEY `bam` (`apMaterno`),
  KEY `bln` (`lugarNacimiento`),
  KEY `bfecha` (`procFechaReg`),
  KEY `btipot` (`tipoTestamento`),
  KEY `bescritura` (`escritura`),
  KEY `nestatus` (`estatus`),
  KEY `numint` (`procNumInt`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";


/////////////////////////////// Creaci�n de la tabla-- permisoxuser -- ///////////////////////////////////////

$permisoxuser = "CREATE TABLE IF NOT EXISTS `permisoxuser` (
  `idPermiso` int(11) NOT NULL DEFAULT '0',
  `idUsr` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPermiso`,`idUsr`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1";


/////////////////////////////// Creaci�n de la tabla-- Testamentos -- ///////////////////////////////////////

$testamentos = "CREATE TABLE IF NOT EXISTS `testamentos` (
  `idTestamento` int(11) NOT NULL AUTO_INCREMENT,
  `idEstado` int(11) NOT NULL DEFAULT '0',
  `idMunicipio` int(11) DEFAULT NULL,
  `idNotario` int(11) DEFAULT NULL,
  `Nombre` varchar(100) NOT NULL,
  `apPaterno` varchar(50) NOT NULL,
  `apMaterno` varchar(50) NOT NULL,
  `apConyuge` varchar(50) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `lugarNacimiento` varchar(200) NOT NULL,
  `edoCivil` enum('SOLTERO','CASADO','DIVORCIADO','UNION LIBRE','VIUDO','SOC. DE CONVIVENCIA U HOMOLOGA') DEFAULT NULL,
  `nacionalidad` varchar(50) NOT NULL,
  `domicilio` text,
  `sexo` enum('Masculino','Femenino') DEFAULT NULL,
  `nomPadre` varchar(100) DEFAULT NULL,
  `apPatPadre` varchar(50) DEFAULT NULL,
  `apMatPadre` varchar(50) DEFAULT NULL,
  `nomMadre` varchar(100) DEFAULT NULL,
  `apPatMadre` varchar(50) DEFAULT NULL,
  `apMatMadre` varchar(50) DEFAULT NULL,
  `apConMadre` varchar(50) DEFAULT NULL,
  `tipoTestamento` varchar(50) NOT NULL,
  `escritura` mediumint(9) DEFAULT NULL,
  `fechaInstrumento` date DEFAULT NULL,
  `horaInstrumento` time DEFAULT '00:00:00',
  `juez` varchar(150) NOT NULL,
  `lugarOtorgamiento` varchar(100) NOT NULL,
  `oTomo` varchar(100) DEFAULT NULL,
  `ofiRegistro` varchar(100) NOT NULL,
  `capturista` varchar(50) NOT NULL,
  `revisor` varchar(50) NOT NULL,
  `valido` varchar(50) NOT NULL,
  `procFechaReg` date DEFAULT NULL,
  `horaRegistro` time DEFAULT NULL,
  `procNumInt` varchar(100) DEFAULT NULL,
  `procNumIntEdo` varchar(100) NOT NULL,
  `fechaIngreso` date NOT NULL DEFAULT '0000-00-00',
  `fechaRegistro` date NOT NULL DEFAULT '0000-00-00',
  `estatus` enum('local','nacional') DEFAULT 'local',
  `notas` mediumtext,
  `curp` varchar(30) DEFAULT NULL,
  `contenidoIrrevocable` enum('Si','No') DEFAULT NULL,
  PRIMARY KEY (`idTestamento`),
  KEY `municipioTestamento_FK` (`idEstado`,`idMunicipio`),
  KEY `notarioTestamento_FK` (`idNotario`),
  KEY `escritura_FK` (`escritura`),
  KEY `bnombre` (`Nombre`),
  KEY `bapp` (`apPatPadre`),
  KEY `bapm` (`apPatMadre`),
  KEY `bnp` (`nomPadre`),
  KEY `bnm` (`nomMadre`),
  KEY `bap` (`apPaterno`),
  KEY `bamp` (`apMatPadre`),
  KEY `bamm` (`apMatMadre`),
  KEY `bnac` (`nacionalidad`),
  KEY `bam` (`apMaterno`),
  KEY `bln` (`lugarNacimiento`),
  KEY `bfecha` (`procFechaReg`),
  KEY `btipot` (`tipoTestamento`),
  KEY `bescritura` (`escritura`),
  KEY `nestatus` (`estatus`),
  KEY `numint` (`procNumInt`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";


/////////////////////////////// Creaci�n de la tabla-- User -- ///////////////////////////////////////

$user = "CREATE TABLE IF NOT EXISTS `user` (
  `idUsr` int(11) NOT NULL AUTO_INCREMENT,
  `idEstado` int(11) NOT NULL DEFAULT '0',
  `user` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `apPaterno` varchar(25) NOT NULL,
  `apMaterno` varchar(25) NOT NULL,
  `puesto` varchar(255) NOT NULL,
  `dependencia` varchar(255) NOT NULL,
  `activo` enum('Si','No') NOT NULL DEFAULT 'Si',
  `fechaAlta` date NOT NULL DEFAULT '0000-00-00',
  `padre` int(11) NOT NULL DEFAULT '1',
  `tipo` enum('DEPENDENCIA','NOTARIA') NOT NULL,
  `idNotario` int(5) DEFAULT NULL,
  PRIMARY KEY (`idUsr`),
  KEY `padres` (`idUsr`,`padre`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";

$r1 = mysql_query($alias) or die(mysql_error());
$r2 = mysql_query($bitacora) or die(mysql_error());
$r3 = mysql_query($catestado) or die(mysql_error());
$r4 = mysql_query($catmunicipios) or die(mysql_error());
$r5 = mysql_query($catnotarios) or die(mysql_error());
$r6 = mysql_query($catpermisos) or die(mysql_error());
$r7 = mysql_query($cattiponot) or die(mysql_error());
$r8 = mysql_query($consultas) or die(mysql_error());
$r9 = mysql_query($modificaciones) or die(mysql_error());
$r10 = mysql_query($permisoxuser) or die(mysql_error());
$r11 = mysql_query($testamentos) or die(mysql_error());
$r12 = mysql_query($user) or die(mysql_error());
	
	echo "<br><br><center>Se ha creado la base de datos y las tablas exitosamente. Por favor de clic en siguiente para llenar la base de datos con informaci�n</center><br>";
	echo "	<form action='instalador2.php' method='post'>
	<input type='hidden' name='tipo' value='llenar'/>
	<input type='hidden' name='base' value='$base'/>
	<input type='hidden' name='servidor' value='$servidor'/>
	<input type='hidden' name='usuario' value='$usuario'/>
	<input type='hidden' name='pass' value='$pass'/>
	<center><input type='submit' name='Llenar' value='Llenar la base de datos' /></center>
	</form>";
	
	} 
	else 
	{
	echo "<center> Hubo un error al tratarse de conectar al servidor</center>";
	}
	

 } ?>	
	<br /><br /><br />
	</td>
  </tr>
</table>



</body>
</html>
